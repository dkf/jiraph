TARGET      = keygen
TEMPLATE    = app
CONFIG      += console
SOURCES     += main.cpp
QT -= gui

CONFIG(release, debug|release) {
    message(Disabling debug output.)
    DEFINES += QT_NO_DEBUG_OUTPUT
}

win32: {
  include(winconf.pri)
}

HEADERS +=  ../src/crypto.h \
            ../src/simplecrypt.h

SOURCES +=  ../src/crypto.cpp \
            ../src/simplecrypt.cpp
