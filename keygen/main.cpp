#include <QtCore/QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QTextCodec>
#include <QDebug>
#include "../src/crypto.h"

const int ERC_SUCCESS   = 0;
const int ERC_SUCCESS_BIN = 1;
const int ERC_ERROR     = 10;
const int ERC_MEMORY    = 11;
const int ERC_FILE_IO   = 12;
const int ERC_BAD_ARGS  = 13;
const int ERC_BAD_INPUT = 14;
const int ERC_EXPIRED   = 15;
const int ERC_INTERNAL  = 16;

void report_error(const QString& filename, const QString& message)
{
    QFile errfile(filename);
    if (errfile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream ts(&errfile);
        ts << message;
    }
}

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    if (argc != 4)
        return ERC_BAD_ARGS;

    int res = ERC_ERROR;

    QString input_file  = QString::fromAscii(argv[1]);
    QString error_file  = QString::fromAscii(argv[2]);
    QString output_file = QString::fromAscii(argv[3]);

    QFile in_file(input_file);    
    QString key;

    if (in_file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&in_file);
        QTextCodec* tc = QTextCodec::codecForName("UTF-8");

        while (!in.atEnd())
        {
            QStringList strings = in.readLine().split("=");

            if(strings.size() < 2)
                continue;

            qDebug() << "Key: " << strings.at(0);

            if (strings.at(0) == QString::fromUtf8("ENCODING"))
            {
                qDebug() << "reset to utf8";
                in.setCodec(tc);
                continue;
            }

            if (strings.at(0) == "REG_NAME")
            {
                // generate
                qDebug() << "build key on " << strings.at(1);
                key = crypto::pkey(strings.at(1));
                break;
            }
        }

        in_file.close();

        if (key.isEmpty())
        {
            // write error output
            report_error(error_file, QString::fromUtf8("Unable to find tag REG_NAME"));
            res = ERC_BAD_INPUT;
        }
        else
        {
            // ok
            QFile outfile(output_file);

            if (outfile.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                QTextStream ts(&outfile);
                ts << key;
                res = ERC_SUCCESS;
            }
            else
            {
                report_error(error_file, QString::fromUtf8("Unable to open file: %1").arg(output_file));
                res = ERC_FILE_IO;
            }
        }
    }
    else
    {
        report_error(error_file, QString::fromUtf8("Unable to open file: %1").arg(input_file));
        res = ERC_FILE_IO;
    }

    return res;
}


