Which language / compiler did you use for the creation of this key generator:
C++/Microsoft Visual Studio 2008 Express Edition

Which version of this specification did you refer to when writing the code? (see beginning of this document):
Key Generator SDK
Version 3.9 / June 2012

Which auxiliary files (DLL's, Runtime Environment, etc.) are required for your generator to work:
QtCore4.dll and VS runtime ddls. All required files already in keygen directory.

Which temporary and/or persistent files are created by the generator:
Nothing

Are there any limitations regarding input values:
Keygen uses REG_NAME and ENCODING variables from input file. Input parameters count must be exactly the three.