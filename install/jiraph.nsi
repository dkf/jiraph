!define QT_DIR "C:\QtSDK\Desktop\Qt\4.8.1\msvc2008"
!define SSL_DIR "D:\dev\OpenSSL\OpenSSL"
!define VC_REDIST "'C:\Program Files\Microsoft Visual Studio 9.0\VC\redist\x86\Microsoft.VC90.CRT'"
!define PRODUCT_NAME "Jiraph"
!define PRODUCT_VERSION "0.6"

XPStyle on
CRCCheck On
InstallDir $PROGRAMFILES\Jiraph
Caption "${PRODUCT_NAME} Ver. ${PRODUCT_VERSION}"

Icon "../src/icons/timer.ico"
OutFile "${PRODUCT_NAME}_install.exe"

!ifdef NOCOMPRESS
SetCompress off
!endif

AutoCloseWindow false
ShowInstDetails hide

;--------------------------------

; First is default
LoadLanguageFile "${NSISDIR}\Contrib\Language files\English.nlf"
LoadLanguageFile "${NSISDIR}\Contrib\Language files\Russian.nlf"


; License data
; Not exactly translated, but it shows what's needed
LicenseLangString myLicenseData ${LANG_ENGLISH} "lics\lic_en.txt"
LicenseLangString myLicenseData ${LANG_RUSSIAN} "lics\lic_ru.txt"

LicenseData $(myLicenseData)

; Set name using the normal interface (Name command)
LangString Name ${LANG_ENGLISH} "${PRODUCT_NAME}"
LangString Name ${LANG_RUSSIAN} "${PRODUCT_NAME}"

Name $(Name)

; Directly change the inner lang strings (Same as ComponentText)
LangString ^ComponentsText ${LANG_ENGLISH} "�omponent page"
LangString ^ComponentsText ${LANG_RUSSIAN} "�������� ���������� ��� ���������"

; Set one text for all languages (simply don't use a LangString)

LangString CompletedText ${LANG_ENGLISH} "${PRODUCT_NAME} installer completed"
LangString CompletedText ${LANG_RUSSIAN} "��������� ${PRODUCT_NAME} ���������"

LangString InstQuestion ${LANG_ENGLISH} "Install components?"
LangString InstQuestion ${LANG_RUSSIAN} "���������� ����������?"

LangString UnInstWarning ${LANG_ENGLISH} "Unable to remove directory: "
LangString UnInstWarning ${LANG_RUSSIAN} "�� ���� ������� ����������: "

LangString Main      ${LANG_ENGLISH} "Main ${PRODUCT_NAME} files"
LangString Main      ${LANG_RUSSIAN} "�������� ����� ${PRODUCT_NAME}"

LangString QTDLL    ${LANG_ENGLISH} "Qt4 dll"
LangString QTDLL    ${LANG_RUSSIAN} "Qt4 dll"

LangString SSLDLL    ${LANG_ENGLISH} "OpenSSL dll"
LangString SSLDLL    ${LANG_RUSSIAN} "OpenSSL dll"


LangString ComponentsTitle ${LANG_ENGLISH} "Select components for install"
LangString ComponentsTitle ${LANG_RUSSIAN} "�������� ���������� ��� ���������"


PageEx license 
	LicenseForceSelection checkbox
	LicenseData $(myLicenseData)
 PageExEnd
 
PageEx components
	ComponentText $(ComponentsTitle)
PageExEnd

Page directory
Page instfiles beginInst
 
UninstPage uninstConfirm
UninstPage instfiles
 
Section "" ; empty string makes it hidden, so would starting with -

  ; write reg info
  WriteRegStr HKLM "SOFTWARE\${PRODUCT_NAME}" "Install_Dir" "$INSTDIR"

  ; write uninstall strings
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "DisplayName" "${PRODUCT_NAME} (remove only)"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}" "UninstallString" "$INSTDIR\${PRODUCT_NAME}-uninst.exe"
  SetOutPath $INSTDIR
  
  ;create desktop shortcut
  CreateShortCut "$DESKTOP\${PRODUCT_NAME}.lnk" "$INSTDIR\${PRODUCT_NAME}.exe" ""
 
  ;create start-menu items
  CreateDirectory "$SMPROGRAMS\${PRODUCT_NAME}"
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\Uninstall.lnk" "$INSTDIR\${PRODUCT_NAME}-uninst.exe" "" "$INSTDIR\${PRODUCT_NAME}-uninst.exe" 0
  CreateShortCut "$SMPROGRAMS\${PRODUCT_NAME}\${PRODUCT_NAME}.lnk" "$INSTDIR\${PRODUCT_NAME}.exe" "" "$INSTDIR\${PRODUCT_NAME}.exe" 0
 
  WriteUninstaller "${PRODUCT_NAME}-uninst.exe"
  
  Nop ; for fun

SectionEnd

Section $(Main)
	SetOutPath $INSTDIR
	File /oname=${PRODUCT_NAME}.exe ..\src\release\jiraph.exe
	File redist\Microsoft.VC90.CRT.manifest
	File redist\msvcm90.dll
	File redist\msvcp90.dll
	File redist\msvcr90.dll
SectionEnd

Section $(QTDLL)
	SetOutPath $INSTDIR
	File	${QT_DIR}\bin\QtCore4.dll
	File	${QT_DIR}\bin\QtGui4.dll
	File	${QT_DIR}\bin\QtNetwork4.dll
	
	SetOutPath $INSTDIR\imageformats
	File ${QT_DIR}\plugins\imageformats\qico4.dll
	File ${QT_DIR}\plugins\imageformats\qmng4.dll
SectionEnd

Section $(SSLDLL)
	SetOutPath $INSTDIR
	File	${SSL_DIR}\bin\ssleay32.dll
	File	${SSL_DIR}\bin\libeay32.dll
	;File	${SSL_DIR}\bin\libssl32.dll
SectionEnd


Function beginInst
	MessageBox MB_YESNO $(InstQuestion) IDYES yes
     Abort
   yes:
FunctionEnd


Function .onInit

	;Language selection dialog

	Push ""
	Push ${LANG_ENGLISH}
	Push English
	Push ${LANG_RUSSIAN}
	Push Russian	
	Push A ; A means auto count languages
	       ; for the auto count to work the first empty push (Push "") must remain
	LangDLL::LangDialog "Installer Language" "Please select the language of the installer"

	Pop $LANGUAGE
	StrCmp $LANGUAGE "cancel" 0 +2
		Abort
FunctionEnd

; Uninstaller

UninstallText "This will uninstall obd2 example. Hit next to continue."
UninstallIcon "${NSISDIR}\Contrib\Graphics\Icons\nsis1-uninstall.ico"

;--------------------------------    
;Uninstaller Section  
Section "Uninstall"
 
;Delete Files 
  RMDir /r "$INSTDIR\*.*"    
 
;Remove the installation directory
  RMDir "$INSTDIR"
 
;Delete Start Menu Shortcuts
  Delete "$DESKTOP\${PRODUCT_NAME}.lnk"
  Delete "$SMPROGRAMS\${PRODUCT_NAME}\*.*"
  RmDir  "$SMPROGRAMS\${PRODUCT_NAME}"
 
;Delete Uninstaller And Unistall Registry Entries
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\${PRODUCT_NAME}"
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT_NAME}"  
 
 	IfFileExists "$INSTDIR" 0 NoErrorMsg
		MessageBox MB_OK "$(UnInstWarning) $INSTDIR !" IDOK 0 ; skipped if file doesn't exist
	NoErrorMsg:
SectionEnd
