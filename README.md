#JIRAPH - handy desktop time tracking client for JIRA®#
## Developed in C++/Qt ##
* It is fast, you won't wait while JIRA® responds
* Track your time with just few convenient clicks
* Tracking time more accurately than with JIRA®'s tools
* Customize your comments policy, use work reminders and idle detection
![screen2.png](https://bitbucket.org/repo/b7erAn/images/3479392629-screen2.png)
![screen3.png](https://bitbucket.org/repo/b7erAn/images/1127828165-screen3.png)
![logwork1.png](https://bitbucket.org/repo/b7erAn/images/2554795397-logwork1.png)