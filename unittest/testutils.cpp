#include <QtTest/QtTest>
#include "../src/utils.h"
#include "testutils.h"

TestUtils::TestUtils()
{
}

void TestUtils::testFriendlyHours()
{
    QCOMPARE(QString(""), utils::userFriendlyHours(0));
    QCOMPARE(QString("10s"), utils::userFriendlyHours(10));
    QCOMPARE(QString("2m 1s"), utils::userFriendlyHours(121));
    QCOMPARE(QString("1h "), utils::userFriendlyHours(3600));
    QCOMPARE(QString("59m 59s"), utils::userFriendlyHours(3599));
    QCOMPARE(QString("144h 10m 20s"), utils::userFriendlyHours(144*3600 + 60*10 + 20));
}

void TestUtils::testUrl2IssueId()
{
    QCOMPARE(10000LL, utils::url2issueid(QString("http://87.251.145.50:8888/jira/rest/api/2/issue/10000/worklog/10006")));
    QCOMPARE(10001LL, utils::url2issueid(QString("http://87.251.145.50/jira/rest/api/2/issue/10001/worklog/10006")));
    QCOMPARE(10002LL, utils::url2issueid(QString("http://87.251.145.50:8888/api2/issue/10002/worklog/10006")));
    QCOMPARE(10003LL, utils::url2issueid(QString("http://87.251.145.50:8888////api/2/issue/10003/worklog/10006")));
    QCOMPARE(-1LL, utils::url2issueid(QString("http://87.251.145.50:8888/jira/rest/api/2/issu/10000/worklog/10006")));
    QCOMPARE(-1LL, utils::url2issueid(QString("http://87.251.145.50:8888/jira/rest/api/2/issue10000/worklog/10006")));
    QCOMPARE(10000LL, utils::url2issueid(QString("http://87.251.145.50/jira/rest/api/2/issue/10000")));
}
