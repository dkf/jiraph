#ifndef TESTUTILS_H
#define TESTUTILS_H

#include <QObject>

class TestUtils : public QObject
{
    Q_OBJECT
public:
    TestUtils();
private slots:
    void testFriendlyHours();
    void testUrl2IssueId();
};

#endif // TESTUTILS_H
