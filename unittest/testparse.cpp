#include <QtTest/QtTest>
#include <QFile>
#include <QTextStream>
#include <QRegExp>
#include <QStringList>

#include "../src/json/json.h"
#include "../src/jiraresource.h"
#include "../src/utils.h"
#include "../src/crypto.h"
#include "testparse.h"

TestParse::TestParse()
{
    str_test_list << qMakePair(qMakePair(QString("1h"), QString("1h")), true)
                << qMakePair(qMakePair(QString("1h1m1d"), QString("1h 1m 1d")), true)
                << qMakePair(qMakePair(QString("0.1h"), QString("0.1h")), true)
                << qMakePair(qMakePair(QString("1.1h"), QString("1.1h")), true)
                << qMakePair(qMakePair(QString("1.1m"), QString("1.1m")), true)
                << qMakePair(qMakePair(QString(".1h"), QString(".1h")), true)
                << qMakePair(qMakePair(QString("1 h 1  d 1m"), QString("1h 1d 1m")), true)
                << qMakePair(qMakePair(QString("1.1 h  2h 2h .1m"), QString("1.1h 2h 2h .1m")), true)
                << qMakePair(qMakePair(QString("1w10d2.0h.1m"), QString("1w 10d 2.0h .1m")), true)
                << qMakePair(qMakePair(QString("1     h       3   d    5h"), QString("1h 3d 5h")), true)
                << qMakePair(qMakePair(QString("1     h       3   d    5h    "), QString("1h 3d 5h")), true)
                << qMakePair(qMakePair(QString("    15h   "), QString("15h")), true)

                << qMakePair(qMakePair(QString("1.h"), QString("")), false)
                << qMakePair(qMakePair(QString("1k"), QString("")), false)
                << qMakePair(qMakePair(QString("h"), QString("")), false)
                << qMakePair(qMakePair(QString("2.0g"), QString("")), false)
                << qMakePair(qMakePair(QString("h1"), QString("")), false)
                << qMakePair(qMakePair(QString("1   h   m"), QString("")), false)
                << qMakePair(qMakePair(QString("1.  0 h"), QString("")), false)
                << qMakePair(qMakePair(QString(""), QString("")), false)
                << qMakePair(qMakePair(QString("      "), QString("")), false)
                << qMakePair(qMakePair(QString("   1h   1   "), QString("")), false);
}

void TestParse::parseCommon()
{    
    QFile f("issuelist.json");
    if (!f.open(QFile::ReadOnly | QFile::Text))
    {
        QFAIL("Unable to find test input data");
    }


    QTextStream in(&f);
    QString input = in.readAll();
    QVERIFY(!input.isEmpty());

    bool ok;
    QtJson::JsonObject js = QtJson::parse(input, ok).toMap();

    if (!ok) QFAIL("Unable to parse json");

    QList<JiraIssue> issues = json2issueList(js);

    QCOMPARE(issues.size(), 5);
}

void TestParse::parseFilter()
{
    QFile f("filterlist.json");

    if (!f.open(QFile::ReadOnly | QFile::Text))
    {
        QFAIL("Unable to find test input data");
    }


    QTextStream in(&f);
    QString input = in.readAll();
    QVERIFY(!input.isEmpty());

    bool ok;
    QVariant js = QtJson::parse(input, ok);

    if (!ok) QFAIL("Unable to parse json");

    QList<JiraFilter> filters = json2filterList(js);

    QCOMPARE(filters.size(), 1);

    QCOMPARE(filters.at(0).id, 32533);
    QCOMPARE(filters.at(0).self, QString::fromUtf8("https://www.rocketrack.com/rest/api/2/filter/32533"));
    QCOMPARE(filters.at(0).name, QString::fromUtf8("Assigned to me"));
    QCOMPARE(filters.at(0).jql, QString::fromUtf8("project = ARYMP AND assignee in (currentUser()) ORDER BY priority DESC, created DESC"));
    QCOMPARE(filters.at(0).viewUrl, QString::fromUtf8("https://www.rocketrack.com/secure/IssueNavigator.jspa?mode=hide&requestId=32533"));
    QCOMPARE(filters.at(0).searchUrl, QString::fromUtf8("https://www.rocketrack.com/rest/api/2/search?jql=project+%3D+ARYMP+AND+assignee+in+(currentUser())+ORDER+BY+priority+DESC,+created+DESC"));
}

void TestParse::testWorklogRegex()
{
    QRegExp rx("(\\s*\\d*\\.?\\d+\\s*[wdhm]\\s*)+");
    QPair<QPair<QString,QString>, bool> p;

    foreach(p, str_test_list)
    {
        if (p.second)
            QVERIFY(rx.exactMatch(p.first.first));
        else
            QVERIFY(!rx.exactMatch(p.first.first));
    }
}


void TestParse::testNormalize()
{
    QPair<QPair<QString,QString>, bool> p;

    foreach(p, str_test_list)
    {
        if (p.second)
        QCOMPARE(utils::normalize(p.first.first), p.first.second);
    }
}

void TestParse::testCipher()
{
    QCOMPARE(crypto::pkey("Test"), QString::fromUtf8("D98C21B8EABF1F2D5C27109CBF6B255F"));
    QCOMPARE(crypto::pkey("SomeId"), QString::fromUtf8("BE596144885A7322109CE2694D119415"));
    QCOMPARE(crypto::pkey("xxcxc223323232"), QString::fromUtf8("1452D24D217BC08A5F47138A0D580B89"));
    QCOMPARE(crypto::pkey("windows23232323232"), QString::fromUtf8("D841106C7FE3278AB129905FCBC372F6"));
    QCOMPARE(crypto::pkey("1"), QString::fromUtf8("831DD9DD3815E27D108F74C2EEDAC40B"));
    QCOMPARE(crypto::pkey(""), QString::fromUtf8("6DBB32EF949E7EB7DB5CFA969D7E3316"));
    QCOMPARE(crypto::pkey("\\//"), QString::fromUtf8("1E99F4B504B8CDAD74FE1A113BFB6DCF"));

}
