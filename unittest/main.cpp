#include <QApplication>
#include <QTest>
#include <iostream>
#include <cstdlib>
#include <cstdio>
#include "testparse.h"
#include "testmerge.h"
#include "testutils.h"

int main(int argc, char *argv[])
{
    freopen("testing.log", "w", stdout);
    QApplication a(argc, argv);
    QTest::qExec(new TestParse, argc, argv);
    QTest::qExec(new TestMerge, argc, argv);
    QTest::qExec(new TestUtils, argc, argv);
    return 0;
}
