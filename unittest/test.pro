HEADERS +=  ../src/jiraresource.h \
            ../src/utils.h \
            ../src/simplecrypt.h \
            ../src/crypto.h \
            ../src/preferences/preferences.h \
            testparse.h \
            testmerge.h \
            testutils.h

SOURCES +=  ../src/jiraresource.cpp \
            ../src/utils.cpp \
            ../src/simplecrypt.cpp \
            ../src/crypto.cpp \
            ../src/preferences/preferences.cpp \
            testparse.cpp \
            main.cpp \
            testmerge.cpp \
            testutils.cpp

CONFIG  += qtestlib

INCLUDEPATH += ../src ../src/preferences

include (../version.pri)
include (../src/json/json.pri)

