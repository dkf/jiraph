#ifndef __TEST_PARSE__
#define __TEST_PARSE__

#include <QObject>
#include <QList>
#include <QPair>

class TestParse: public QObject
{
     Q_OBJECT
public:
    TestParse();
private:
    QList<QPair<QPair<QString, QString>, bool> >    str_test_list;
private slots:
     void parseCommon();
     void parseFilter();
     void testWorklogRegex();
     void testNormalize();
     void testCipher();
};

#endif
