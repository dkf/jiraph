#include <QtTest/QtTest>
#include <QList>
#include <QDebug>
#include "testmerge.h"

void TestMerge::testMerge()
{
    QList<int> base;
    base << 1 << 2 << 5 << 3 << 8 << 0 << 4 << 6 << 9 << 7 << 10 << 11 << 12 << 14 << 13 << 15;
    qSort(base.begin(), base.end());

    QList<int> in;
    in << 3 << 2 << 4 << 1 << 0 << 10 << 11 << 12 << 16 << 17 << -1 << -2 << -3;


    // remove extra elements
    QList<int>::iterator itr = base.begin();

    while(itr != base.end())
    {
        if (in.contains(*itr))
        {
            ++itr;
        }
        else
        {
            itr = base.erase(itr);
        }
    }

    QList<int> after_clean;
    after_clean << 0 << 1 << 2 << 3 << 4 << 10 << 11 << 12;
    qDebug() << base;
    QCOMPARE(base, after_clean);
    QCOMPARE(qLowerBound(base.begin(), base.end(), 13), base.end());

    // insert new elements
    foreach(int i, in)
    {
        QList<int>::iterator pos = qLowerBound(base.begin(), base.end(), i);

        if (pos == base.end() || *pos != i)
        {
            base.insert(pos, i);
        }
    }

    QList<int> after_merge;
    after_merge << -3 << -2 << -1 << 0 << 1 << 2 << 3 << 4 << 10 << 11 << 12 << 16 << 17;
    qDebug() << base;
    QCOMPARE(base, after_merge);
    QCOMPARE(base.end() - base.begin(), base.size());
    QCOMPARE(qLowerBound(base.begin(), base.end(), 0) - base.begin(), 3);

}
