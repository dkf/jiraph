#ifndef __TESTMERGE__
#define __TESTMERGE__

#include <QObject>

class TestMerge : public QObject
{
    Q_OBJECT
private slots:
    void testMerge();
};

#endif // __TESTMERGE__
