#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <xcb/xcb.h>
#include <xcb/screensaver.h>

static xcb_connection_t * connection;
static xcb_screen_t * screen;


/**
 * Connects to the X server (via xcb) and gets the screen
  */
void magic_begin () 
{
   connection = xcb_connect (NULL, NULL);
   screen = xcb_setup_roots_iterator (xcb_get_setup (connection)).data;
}

/**
* Asks X for the time the user has been idle
* @returns idle time in milliseconds
 */
 unsigned long magic_get_state () 
{
    xcb_screensaver_query_info_cookie_t cookie;
    xcb_screensaver_query_info_reply_t *info;

    cookie = xcb_screensaver_query_info (connection, screen->root);
    info = xcb_screensaver_query_info_reply (connection, cookie, NULL);

//    int state = info->state;
    int state = info->ms_since_user_input;

    return state;
}


int main(int arc, char *argv[])
{
    magic_begin();    
    while(1)
    {        
        int state = magic_get_state();
        printf("state: %d\n", state);
        sleep(1);
    }
}

// build line
// g++ -I/home/apavlov/local/include -L/home/apavlov/local/lib -lxcb-screensaver screen.cpp -o screen

