#!/bin/sh

#if [ $# -lt 1 ]
#then
#	echo "Set version in first parameter"
#	exit 1
#fi

JIRAPH=jiraph
ICON_FILE=timer.png
DESKTOP_FILE=jiraph.desktop

SRC_DIR=src
PKG_DIR=package
APP_DIR=/usr/bin
MENU_DIR=/usr/share/applications
ICONS_DIR=/usr/share/app-install/icons
PKG_APP_DIR=${PKG_DIR}${APP_DIR}
PKG_MENU_DIR=${PKG_DIR}${MENU_DIR}
PKG_ICONS_DIR=${PKG_DIR}${ICONS_DIR}

ARCH=`uname -i`
LB=`getconf LONG_BIT`
CTRL=${PKG_DIR}/DEBIAN/

if [ $LB -eq "64" ]
then
ARCH="amd64"
fi

# checks whether specified file exists. if doesn't then exits
# $1 - path to file to be checked
ensureFileExists() 
{
    if [ ! -f "$1" ]; then
        echo "File doesn't exist: $1"
        exit 1
    fi
}

ensureFileExists "${SRC_DIR}/$JIRAPH"
ensureFileExists "${SRC_DIR}/icons/$ICON_FILE"

VERSION=$(${SRC_DIR}/$JIRAPH --version | awk '{print $1}')

# cleanup
rm -f *.deb
rm -rf ${PKG_DIR}/*
# create directories
mkdir -m 755 -p ${CTRL}
mkdir -m 755 -p ${PKG_APP_DIR}
mkdir -m 755 -p ${PKG_MENU_DIR}
mkdir -m 755 -p ${PKG_ICONS_DIR}
chmod -R 755 ${PKG_DIR}/*

JIRAPH_DESKTOP=${PKG_MENU_DIR}/$DESKTOP_FILE

# generating a desktop file
echo "[Desktop Entry]" > $JIRAPH_DESKTOP
echo "Name=Jiraph" >> $JIRAPH_DESKTOP
echo "GenericName=Handy desktop time tracking client for JIRA" >> $JIRAPH_DESKTOP
echo "Comment=Helps JIRA users track their work time and manipulate tickets without accessing JIRA via web browser" >> $JIRAPH_DESKTOP
echo "Exec=/usr/bin/jiraph" >> $JIRAPH_DESKTOP
echo "Icon=${ICONS_DIR}/$ICON_FILE" >> $JIRAPH_DESKTOP
echo "Terminal=false" >> $JIRAPH_DESKTOP
echo "X-MultipleArgs=false" >> $JIRAPH_DESKTOP
echo "Type=Application" >> $JIRAPH_DESKTOP
echo "Categories=Development" >> $JIRAPH_DESKTOP

cp -v "${SRC_DIR}/icons/$ICON_FILE" "${PKG_ICONS_DIR}/$ICON_FILE"
cp -v "${SRC_DIR}/$JIRAPH" "${PKG_APP_DIR}/$JIRAPH"
cd "${PKG_DIR}" 
md5deep -l -r "usr" > "../${CTRL}/md5sums" 
cd ..
SZ=$(du -s -k ${PKG_DIR}/usr | awk '{print $1}')
# generate control

echo "Package: $JIRAPH" > "${CTRL}/control"
echo "Version: $VERSION" >> "${CTRL}/control"
echo "Architecture: $ARCH " >> "${CTRL}/control"
echo "Maintainer: DKF <dkfsoft@gmail.com>" >> "${CTRL}/control"
echo "Installed-Size: $SZ" >> "${CTRL}/control"
echo "Depends: libc6 (>= 2.14), libgcc1 (>= 1:4.1.1), libqt4-dbus (>= 4:4.5.3), libqt4-network (>= 4:4.5.3), libqtcore4 (>= 4:4.8.0), libqtgui4 (>= 4:4.8.0), libstdc++6 (>= 4.6), libxcb1, libxcb-screensaver0" >> "${CTRL}/control"
echo "Suggests: " >> "${CTRL}/control"
echo "Section: unknown" >> "${CTRL}/control"
echo "Priority: optional" >> "${CTRL}/control"
echo "Homepage: http://dkfsoft.com" >> "${CTRL}/control"
echo "Description: Handy desktop time tracking client for JIRA" >> "${CTRL}/control"

# setup permissions
chmod 755 ${PKG_APP_DIR}/${JIRAPH}
chmod 644 ${CTRL}/*
chmod 644 ${PKG_ICONS_DIR}/*
chmod 644 ${PKG_MENU_DIR}/*

dpkg-deb -b package "./${JIRAPH}_v${VERSION}_${ARCH}.deb"

