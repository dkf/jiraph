#include "logworkdialog.h"
#include "ui_logworkdialog.h"
#include "jiraclient.h"
#include "utils.h"
#include "preferences.h"
#include <QPixmap>
#include <QDialogButtonBox>
#include <QPushButton>

LogWorkDialog::LogWorkDialog(JiraClient* jr, const QString& key, qint32 duration, QWidget *parent) :
    QDialog(parent), ui(new Ui::LogWorkDialog), jira(jr), goodWorkLog(false)
{
    ui->setupUi(this);
    ui->labelIssue->setText(key);
    ui->dateTimeEdit->setDateTime( QDateTime::currentDateTime() );
    ui->labelIcon->setPixmap(QPixmap(":bad_state.ico", 0, Qt::AutoColor));
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(false);
    ui->buttonBox->button(QDialogButtonBox::Ok)->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOkButton));
    ui->buttonBox->button(QDialogButtonBox::Cancel)->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));

    if (duration > 0)
        ui->editTimeSpent->setText(utils::userFriendlyDuration(duration));

    connect(ui->editTimeSpent, SIGNAL(textChanged(QString)), this, SLOT(handleChangeTimeSpent(QString)));
    connect(ui->editComment, SIGNAL(textChanged()), this, SLOT(restateControls()));
}

LogWorkDialog::~LogWorkDialog()
{
    delete ui;
}

void LogWorkDialog::accept()
{
    QString issue = ui->labelIssue->text();
    QString timeSpent = utils::normalize(ui->editTimeSpent->text());
    QString comment = ui->editComment->toPlainText();

    if (!issue.isEmpty() && !timeSpent.isEmpty())
    {
        jira->addWorklog(issue, timeSpent, 0, utils::worklogComment(comment));
        done(0);
    }
}

void LogWorkDialog::handleChangeTimeSpent(const QString& text)
{
    QRegExp rx("(\\s*\\d*\\.?\\d+\\s*[wdhm]\\s*)+");
    bool res = rx.exactMatch(text);    
    ui->labelIcon->setPixmap(res?QPixmap(":good_state.ico", 0, Qt::AutoColor):QPixmap(":bad_state.ico", 0, Qt::AutoColor));
    goodWorkLog = res;
    restateControls();
}

void LogWorkDialog::restateControls()
{
    ui->buttonBox->button(QDialogButtonBox::Ok)->setEnabled(goodWorkLog &&
                                                            (Preferences().getRadioComment() != Preferences::RC_DENY || !ui->editComment->toPlainText().isEmpty()));
}
