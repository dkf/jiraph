#ifndef HTTPCONNECTOR_H
#define HTTPCONNECTOR_H

#include <QtNetwork>

#include "error.h"

class HttpRequest;
class HttpConnector : public QObject
{
    Q_OBJECT

    QNetworkReply* m_reply;
    QNetworkAccessManager* m_manager;
    QByteArray m_replyData;

public:
    HttpConnector();
    ~HttpConnector();
    QByteArray request(const HttpRequest& request);

signals:
    void progress(qint64 bytesReceived, qint64 bytesTotal);
    void requestFinished(QByteArray reply);
    void headersRetrieved(QList<QNetworkReply::RawHeaderPair> headers);
    void requestError(Error error);

public slots:
    void finished(QNetworkReply* netReply);
    void handleError(QNetworkReply::NetworkError networkError);
    void sslErrors(const QList<QSslError>& errors);
};

#endif // HTTPCONNECTOR_H
