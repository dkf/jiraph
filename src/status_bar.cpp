#include "status_bar.h"
#include <QStatusBar>

status_bar::status_bar(QWidget *parent, QStatusBar *bar)
    : QWidget(parent) , m_bar(bar)
{
    setupUi(this);

    m_bar->addPermanentWidget(this, 1);
    m_bar->setContentsMargins(-1, -1, -1, -1);
    m_bar->setFixedHeight(21);
    reset();
}

status_bar::~status_bar()
{
}

void status_bar::setLogin(const QString& text)
{
    labelLogin->setText(tr("Login as: ") + text);
}

void status_bar::setRequest(const QString& text)
{
    labelRequest->setText(text);
}

void status_bar::setLastJql(const QString& text)
{
    labelJql->setText(tr("Jql: ") + text);
}

void status_bar::reset()
{

}
