#ifndef __FILTERLISTMODEL__
#define __FILTERLISTMODEL__

#include <QAbstractListModel>
#include "jiraresource.h"

class FilterListModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum Roles
    {
        JQLRole = Qt::UserRole + 1
    };

    explicit FilterListModel(QObject *parent = 0);
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    void save();
    void load();
private:
    QList<JiraFilter>    filters;
    int itr2pos(const QList<JiraFilter>::const_iterator& itr) const;
signals:
    
public slots:
    void mergeFilters(const QList<JiraFilter>&);
};

#endif // __FILTERLISTMODEL__
