CONFIG      += link_pkgconfig
PKGCONFIG   += xcb xcb-screensaver

dbus {
  QT += dbus
  include(qtnotify/qtnotify.pri)
}

unix:!mac{
  QMAKE_LFLAGS_RPATH=
}


