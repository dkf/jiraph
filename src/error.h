#ifndef ERROR_H
#define ERROR_H

struct Error 
{
    enum Type
    {
        NO_ERROR = 0,
        // http errors
        NETWORK_ERROR,
        PROXY_ERROR,
        AUTH_ERROR,
        PERMISSION_ERROR,
        CONTENT_ERROR,
        PROTOCOL_ERROR,
        // jira errors
        BAD_TRANSITION,
        UNKNOWN_RESULT,
        JSON_ERROR
    };

    int code;
    Type type;
    QString message;

    Error(Type t, const QString& m = ""): code(0), type(t), message(m) {}
    Error(int c, Type t, const QString& m = ""): code(c), type(t), message(m) {}
};

#endif // ERROR_H
