#ifndef __ABOUT__
#define __ABOUT__

#include "ui_about.h"
#include <QFile>
#include <QtGlobal>

class AboutDlg : public QDialog, private Ui::AboutDlg
{
    Q_OBJECT

public:
   ~AboutDlg();
    AboutDlg(QWidget *parent, bool onRegPage);
private slots:
    void restateControls();
    void registerProduct();
};

#endif
