#include "about.h"
#include "utils.h"
#include "preferences.h"
#include "crypto.h"

#include <QMessageBox>

AboutDlg::~AboutDlg()
{
    qDebug("Deleting about dlg");
}

AboutDlg::AboutDlg(QWidget *parent, bool onRegPage): QDialog(parent)
{
    Preferences pref;
    setupUi(this);
    setWindowTitle(tr("About %1 version %2").arg(utils::productName()).arg(pref.productStatus()));
    
    QString about = 
        "<html>"
        "<body>"
            "<h4>%1</h4>"
            "<h4>%2</h4>"
            "<h4>%3</h4>"
            "<h4>%4</h4>"
            "<p>"
                "<a href=http://dkfsoft.com>%4</a>"
            "</p>"
        "</body>"
        "</html>";

    lb_about->setText(
        about.arg("The man-hour registration program for Jira").
              arg("Copyrightę2013 DKF soft").
              arg(QString("Version: %1").arg(utils::productVersion())).
              arg(pref.productStatus()).
              arg(tr("Visit product home page")));

    Q_ASSERT((onRegPage && !pref.isRegistered()) || !onRegPage);

    if (onRegPage)
        tw_tabs->setCurrentIndex(1);

    if (pref.isRegistered())
    {
        tw_tabs->removeTab(1);
    }
    else
    {
        connect(editRegName, SIGNAL(textChanged(QString)), this, SLOT(restateControls()));
        connect(editRegKey, SIGNAL(textChanged(QString)), this, SLOT(restateControls()));
        connect(btnReg, SIGNAL(clicked()), this, SLOT(registerProduct()));
    }

    show();
}

void AboutDlg::restateControls()
{

    btnReg->setEnabled(!editRegName->text().isEmpty() &&
                       !editRegKey->text().isEmpty() && !Preferences().isRegistered());
}

void AboutDlg::registerProduct()
{
    Preferences pref;

    if (crypto::pkey(editRegName->text().trimmed()) == editRegKey->text().trimmed())
    {
        pref.setRegName(editRegName->text().trimmed());
        pref.setRegKey(editRegKey->text().trimmed());

        if (pref.isRegistered(true))
        {
            QMessageBox::information(this, tr("Product key verification"), tr("Product was succesfully registered"));
            restateControls();
        }
        else
        {
            Q_ASSERT(false);
        }
    }
    else
    {
        QMessageBox::warning(this, tr("Product key verification"), tr("Input key not match user name"));
    }
}
