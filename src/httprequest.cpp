#include <QDebug>

#include "httprequest.h"

HttpRequest::HttpRequest(
    Method method, const QUrl& endpoint, const Headers& headers, const Params& params, const QByteArray& data):
    m_method(method), m_endpoint(endpoint), m_headers(headers), m_params(params), m_data(data)
{
    m_endpoint.setQueryItems(params);
}

HttpRequest::Method HttpRequest::method() const
{
    return m_method;
}

const QUrl& HttpRequest::endpoint() const
{
    return m_endpoint;
}

QByteArray HttpRequest::mkPostData(const Params& params)
{
    QByteArray result;

    bool first = true;
    typedef QPair<QString,QString> StringPair;
    foreach (const StringPair& kv, params)
    {
        if (!first)
            result.append("&");
        else
            first = false;

        result.append(
            QUrl::toPercentEncoding(kv.first) + QString("=").toUtf8() + QUrl::toPercentEncoding(kv.second));
    }

    return result;
}
