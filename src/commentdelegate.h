#ifndef __COMMENTDELEGATE__
#define __COMMENTDELEGATE__

#include <QItemDelegate>

class CommentDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit CommentDelegate(QObject *parent = 0);   
    QWidget *createEditor(QWidget *parent, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    void setEditorData(QWidget *editor, const QModelIndex &index) const;
    void setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const;
};

#endif // __COMMENTDELEGATE__
