#ifndef PREFERENCES_H__
#define PREFERENCES_H__

#include <QSettings>
#include <QDateTime>

class QIniSettings : public QSettings
{
    Q_OBJECT
    Q_DISABLE_COPY (QIniSettings)
public:
    QIniSettings(const QString &organization, const QString &application = QString(), QObject *parent = 0 ):
#ifdef Q_WS_WIN
    QSettings(QSettings::IniFormat, QSettings::UserScope, organization, application, parent)
#else
    QSettings(organization, application, parent)
#endif
    {

    }

    QIniSettings(const QString &fileName, Format format, QObject *parent = 0 ) : QSettings(fileName, format, parent) {}

#ifdef Q_WS_WIN
    QVariant value(const QString & key, const QVariant &defaultValue = QVariant()) const
    {
        QString key_tmp(key);
        QVariant ret = QSettings::value(key_tmp);
        if (ret.isNull()) return defaultValue;
        return ret;
    }

    void setValue(const QString &key, const QVariant &val)
    {
        QString key_tmp(key);
        if (format() == QSettings::NativeFormat)  key_tmp.replace("\\", "/");
        QSettings::setValue(key_tmp, val);
    }
#endif
};

#define BOOL_PROPERTY(name, key, default_value) \
void set##name(bool input) { setValue(QString::fromUtf8(key), input); } \
bool get##name() const { return value(QString::fromUtf8(key), default_value).toBool(); }

#define INT_PROPERTY(name, key, default_value) \
void set##name(int input) { setValue(QString::fromUtf8(key), input); } \
int get##name() const { return value(QString::fromUtf8(key), default_value).toInt(); }

#define STR_PROPERTY(name, key, default_value) \
void set##name(QString input) { setValue(QString::fromUtf8(key), input); } \
QString get##name() const { return value(QString::fromUtf8(key), default_value).toString(); }

#define SSTR_PROPERTY(name, key, default_value) \
void set##name(QString input) { setValue(QString::fromUtf8(key), Preferences::encrypt(input)); } \
QString get##name() const { return Preferences::decrypt(value(QString::fromUtf8(key), default_value).toString()); }

#define DT_PROPERTY(name, key) \
void set##name(QDateTime input) { setValue(QString::fromUtf8(key), input); } \
QDateTime get##name() const { return value(QString::fromUtf8(key), QDateTime()).toDateTime(); }


class Preferences : public QIniSettings
{
    Q_DISABLE_COPY(Preferences)
private:
    static QString username;
    static QString password;
    static int status;
public:
    enum { RC_ALLOW = 0, RC_WARN, RC_DENY };
    Preferences();
public:
    // Desktop properties
    BOOL_PROPERTY(StartMinimized, "Preferences/Desktop/StartMinimized", false)
    BOOL_PROPERTY(ExitConfirmation, "Preferences/Desktop/ExitConfirm", false)

    BOOL_PROPERTY(ShowInTray, "Preferences/Desktop/ShowInTray", false)
    BOOL_PROPERTY(MinimizeToTray, "Preferences/Desktop/MinimizeToTray", false)
    BOOL_PROPERTY(CloseToTray, "Preferences/Desktop/CloseToTray", false)

    BOOL_PROPERTY(CheckFailsafe, "Preferences/Behaviour/CheckFailsafe", true)
    INT_PROPERTY(SpinFailsafe, "Preferences/Behaviour/SpinFailsafe", 300) // seconds - 5 min

    bool closeToTray() const { return (getShowInTray() && getCloseToTray()); }
    bool minimizeToTray() const { return (getShowInTray() && getMinimizeToTray()); }

    // Connection properties
    STR_PROPERTY(ServerURL, "Preferences/Connection/ServerURL", QString(""))
    BOOL_PROPERTY(SaveUsername, "Preferences/Connection/SaveUsername", false)
    BOOL_PROPERTY(SavePassword, "Preferences/Connection/SavePassword", false)


    QString getUsername() const;
    void setUsername(QString input, bool persistent);

    QString getPassword() const;
    void setPassword(QString input, bool persistent);

    INT_PROPERTY(Timeout, "Preferences/Connection/Timeout", 15)    
    bool readyToConnect() const;

    // Behaviour properties
    INT_PROPERTY(RadioComment, "Preferences/Behaviour/RadioComment", 0)
    BOOL_PROPERTY(CheckActivity, "Preferences/Behaviour/CheckActivity", false)
    INT_PROPERTY(SpinActivity, "Preferences/Behaviour/SpinActivity", 300) // seconds
    BOOL_PROPERTY(CheckRefreshJira, "Preferences/Behaviour/SpinCheckRefreshJira", false)
    INT_PROPERTY(SpinRefresh, "Preferences/Behaviour/SpinRefreshJira", 20)  // seconds
    BOOL_PROPERTY(CheckReminder, "Preferences/Behaviour/SpinCheckReminder", false)
    INT_PROPERTY(SpinReminder, "Preferences/Behaviour/SpinReminder", 300)   // seconds
    BOOL_PROPERTY(ToggleInProgress, "Preferences/Behaviour/ToggleInProgress", false)

    // Main window properties
    STR_PROPERTY(UserFilter, "UserFilter", QString("")) // this is uses in group MainWindow
    STR_PROPERTY(FilterName, "FilterName", QString(""))//<-/
    INT_PROPERTY(FilterComboIndex, "FilterComboIndex", 0)

    // Annotated Jira issue methods
    INT_PROPERTY(WorkLog, "WorkLog", 0)
    STR_PROPERTY(WorkLogText, "WorkLogText", QString(""))

    INT_PROPERTY(Id, "Id", 0)
    STR_PROPERTY(Key, "Key", QString(""))
    STR_PROPERTY(Self, "Self", QString(""))

    STR_PROPERTY(Summary, "Summary", QString(""))
    STR_PROPERTY(Description, "Description", QString(""))
    INT_PROPERTY(Aggregatetimeestimate, "Aggregatetimeestimate", 0)
    INT_PROPERTY(Aggregatetimeoriginalestimate, "Aggregatetimeoriginalestimate", 0)
    INT_PROPERTY(Aggregatetimespent, "Aggregatetimespent", 0)


    INT_PROPERTY(Timespent, "Timespent", 0)
    INT_PROPERTY(Timeestimate, "Timeestimate", 0)
    INT_PROPERTY(Timeoriginalestimate, "Timeoriginalestimate", 0)
    INT_PROPERTY(Workratio, "Workratio", 0)

    DT_PROPERTY(Created, "Created")
    DT_PROPERTY(Updated, "Updated")
    DT_PROPERTY(Duedate, "Duedate")
    DT_PROPERTY(LastViewed, "LastViewed")

    STR_PROPERTY(Status_first, "Status_first", QString(""))
    STR_PROPERTY(Status_second, "Status_second", QString(""))

    STR_PROPERTY(Priority_first, "Priority_first", QString(""))
    STR_PROPERTY(Priority_second, "Priority_second", QString(""))

    INT_PROPERTY(Progress_first, "Progress_first", 0)
    INT_PROPERTY(Progress_second, "Progress_second", 0)
    INT_PROPERTY(Progress_third, "Progress_third", 0)

    STR_PROPERTY(Assignee_self, "Assignee_self", QString(""))
    STR_PROPERTY(Assignee_name, "Assignee_name", QString(""))
    STR_PROPERTY(Assignee_email, "Assignee_email", QString(""))
    STR_PROPERTY(Assignee_dname, "Assignee_dname", QString(""))

    STR_PROPERTY(Reporter_self, "Reporter_self", QString(""))
    STR_PROPERTY(Reporter_name, "Reporter_name", QString(""))
    STR_PROPERTY(Reporter_email, "Reporter_email", QString(""))
    STR_PROPERTY(Reporter_dname, "Reporter_dname", QString(""))

    STR_PROPERTY(IssueType_self, "IssueType_self", QString(""))
    INT_PROPERTY(IssueType_id, "IssueType_id", 0)
    STR_PROPERTY(IssueType_name, "IssueType_name", QString(""))
    STR_PROPERTY(IssueType_descr, "IssueType_descr", QString(""))
    BOOL_PROPERTY(IssueType_subtask, "IssueType_subtask", false)

    // Jira filter
    INT_PROPERTY(FilterId, "FilterId", 0)
    STR_PROPERTY(FilterNameByUser, "FilterName", QString(""))
    STR_PROPERTY(FilterJql, "FilterJql", QString(""))
    STR_PROPERTY(FilterSelf, "FilterSelf", QString(""))
    STR_PROPERTY(LastJql, "LastJql", QString(""))    // last filter which was used

    // registration
    STR_PROPERTY(RegName, "Preferences/Reg/RegName", QString(""))
    STR_PROPERTY(RegKey, "Preferences/Reg/RegKey", QString(""))
    QString productKey() const;
    bool isRegistered(bool force = false) const;
    QString productStatus() const;
};

#endif
