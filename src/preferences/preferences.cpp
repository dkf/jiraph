#include "preferences.h"
#include "utils.h"
#include "crypto.h"

QString Preferences::username = QString("");
QString Preferences::password = QString("");
int Preferences::status = -1;

Preferences::Preferences() : QIniSettings("dkfsoft",  utils::product_name) {}

QString Preferences::getUsername() const
{
    return username.isEmpty()? crypto::decrypt(value("Preferences/Connection/Username",
                                                    crypto::encrypt(QString(""), utils::common_key)).toString(),
                                               utils::common_key):username;
}

void Preferences::setUsername(QString input, bool persistent)
{
    username = input;
    if (persistent)
        setValue("Preferences/Connection/Username", crypto::encrypt(input, utils::common_key));
    else
        remove("Preferences/Connection/Username");
}

QString Preferences::getPassword() const
{
    return password.isEmpty()?crypto::decrypt(value("Preferences/Connection/Password", crypto::encrypt(QString(""), utils::common_key)).toString(),
                                              utils::common_key):password;
}

void Preferences::setPassword(QString input, bool persistent)
{
    password = input;
    if (persistent)
        setValue("Preferences/Connection/Password", crypto::encrypt(input, utils::common_key));
    else
        remove("Preferences/Connection/Password");
}

bool Preferences::readyToConnect() const
{
    return (!getUsername().isEmpty() && !getServerURL().isEmpty()); // TODO - should I check password?
}

QString Preferences::productKey() const
{
    //return crypto::encrypt(utils::machineId(), utils::reg_key, '\x88');
    return QString();
}

bool Preferences::isRegistered(bool force /* = false*/) const
{
    if (status == -1 || force)
    {
        // check status;
        if (getRegKey() == crypto::pkey(getRegName())) status = 1;
        else status = 0;
    }

    return (status == 1);
}

QString Preferences::productStatus() const
{
    return isRegistered()?QString(""):tr("(unregistered copy)");
}
