#ifndef __OPTIONS_H__
#define __OPTIONS_H__

#include "ui_optionsdlg.h"
#include "jiraclient.h"

class OptionsDlg : public QDialog, private Ui::optionsDlg
{
    Q_OBJECT
public:
    enum {  DESKTOP_PAGE = 0, CONNECTION_PAGE,  BEHAVIOUR_PAGE };
    OptionsDlg(int page, QWidget* parent = 0);
signals:
    void madeChanges();
private:
    bool m_madeChanges;
    QScopedPointer<JiraClient> m_jiraTest;

    void saveOptions();
    void loadOptions();
    bool testBtnStatus() const;
private slots:
    void restateControls();
    void applySettings();
    void acceptChanges();
    void cancelChanges();

    void testConnect();
    void connectOk(const JiraUser& user);
    void connectError(Error error, const QString& message);
};

#endif
