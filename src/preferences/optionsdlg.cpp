#include <QDebug>
#include <QMessageBox>

#include "optionsdlg.h"
#include "preferences.h"
#include "utils.h"

const char* SIZE_KEY = "Preferences/OptionsDlg/Size";
const char* POS_KEY= "Preferences/OptionsDlg/Pos";

OptionsDlg::OptionsDlg(int page, QWidget* parent /*= 0*/) : QDialog(parent)
{
    setupUi(this);    
    setModal(true);

    applyBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogApplyButton));
    okBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogOkButton));
    cancelBtn->setIcon(QApplication::style()->standardIcon(QStyle::SP_DialogCancelButton));

    connect(applyBtn, SIGNAL(clicked()), this, SLOT(applySettings()));
    connect(okBtn, SIGNAL(clicked()), this, SLOT(acceptChanges()));
    connect(cancelBtn, SIGNAL(clicked()), this, SLOT(cancelChanges()));
    connect(testBtn, SIGNAL(clicked()), this, SLOT(testConnect()));

    connect(checkStartMin, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(checkExitConfirm, SIGNAL(toggled(bool)), this, SLOT(restateControls()));

    connect(checkShowSystray, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(checkMinimizeToSysTray, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(checkCloseToSystray, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(checkFailSafe, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(spinFailSafe, SIGNAL(valueChanged(int)), this, SLOT(restateControls()));

    connect(urlEdit, SIGNAL(textChanged(QString)), this, SLOT(restateControls()));
    connect(userEdit, SIGNAL(textChanged(QString)), this, SLOT(restateControls()));
    connect(passwEdit, SIGNAL(textChanged(QString)), this, SLOT(restateControls()));    
    connect(checkSaveName, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(checkSavePassword, SIGNAL(toggled(bool)), this, SLOT(restateControls()));

    connect(radioAllowC, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(radioWarnC, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(radioDenyC, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(checkActivity, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(spinActivity, SIGNAL(valueChanged(int)), this, SLOT(restateControls()));
    connect(checkRefresh, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(spinRefresh, SIGNAL(valueChanged(int)), this, SLOT(restateControls()));
    connect(checkReminder, SIGNAL(toggled(bool)), this, SLOT(restateControls()));
    connect(spinReminder, SIGNAL(valueChanged(int)), this, SLOT(restateControls()));
    connect(checkToggleInProgress, SIGNAL(toggled(bool)), this, SLOT(restateControls()));    

    loadOptions();

    Preferences pref;
    resize(pref.value(QString::fromUtf8(SIZE_KEY), size()).toSize());
    QPoint p = pref.value(QString::fromUtf8(POS_KEY), QPoint()).toPoint();
    if (!p.isNull()) move(p);

    tabWidget->setCurrentIndex(page);
}

void OptionsDlg::saveOptions()
{
    applyBtn->setEnabled(false);
    Preferences pref;
    pref.setStartMinimized(checkStartMin->isChecked());
    pref.setExitConfirmation(checkExitConfirm->isChecked());
    pref.setShowInTray(checkShowSystray->isChecked());
    pref.setMinimizeToTray(checkMinimizeToSysTray->isChecked());
    pref.setCloseToTray(checkCloseToSystray->isChecked());

    pref.setCheckFailsafe(checkFailSafe->isChecked());
    pref.setSpinFailsafe(spinFailSafe->value()*60);

    pref.setServerURL(urlEdit->text());
    pref.setSaveUsername(checkSaveName->isChecked());
    pref.setSavePassword(checkSavePassword->isChecked());
    pref.setUsername(userEdit->text(), checkSaveName->isChecked());
    pref.setPassword(passwEdit->text(), checkSavePassword->isChecked());
    pref.setTimeout(timeoutSpin->value());

    // last page
    pref.setSpinActivity(spinActivity->value()*60); // in seconds
    pref.setCheckActivity(checkActivity->isChecked());
    pref.setCheckRefreshJira(checkRefresh->isChecked());
    pref.setSpinRefresh(spinRefresh->value());
    pref.setCheckReminder(checkReminder->isChecked());
    pref.setSpinReminder(spinReminder->value()*60);
    pref.setToggleInProgress(checkToggleInProgress->isChecked());

    if (radioAllowC->isChecked()) pref.setRadioComment(Preferences::RC_ALLOW);
    if (radioWarnC->isChecked()) pref.setRadioComment(Preferences::RC_WARN);
    if (radioDenyC->isChecked()) pref.setRadioComment(Preferences::RC_DENY);

    emit madeChanges();
}

void OptionsDlg::loadOptions()
{
    Preferences pref;
    checkStartMin->setChecked(pref.getStartMinimized());
    checkExitConfirm->setChecked(pref.getExitConfirmation());

    checkShowSystray->setChecked(pref.getShowInTray());
    checkMinimizeToSysTray->setChecked(pref.getMinimizeToTray());
    checkCloseToSystray->setChecked(pref.getCloseToTray());

    checkFailSafe->setChecked(pref.getCheckFailsafe());
    spinFailSafe->setValue(pref.getSpinFailsafe()/60);

    urlEdit->setText(pref.getServerURL());

    checkSaveName->setChecked(pref.getSaveUsername());
    checkSavePassword->setChecked(pref.getSavePassword());
    userEdit->setText(pref.getUsername().isEmpty()?utils::getUserName():pref.getUsername());
    passwEdit->setText(pref.getPassword());
    timeoutSpin->setValue(pref.getTimeout());

    spinActivity->setValue(pref.getSpinActivity()/60);
    checkActivity->setChecked(pref.getCheckActivity());
    checkRefresh->setChecked(pref.getCheckRefreshJira());
    spinRefresh->setValue(pref.getSpinRefresh());
    checkReminder->setChecked(pref.getCheckReminder());
    spinReminder->setValue(pref.getSpinReminder()/60);
    checkToggleInProgress->setChecked(pref.getToggleInProgress());

    switch(pref.getRadioComment())
    {
    case Preferences::RC_ALLOW: radioAllowC->setChecked(true);
        break;
    case Preferences::RC_WARN: radioWarnC->setChecked(true);
        break;
    case Preferences::RC_DENY: radioDenyC->setChecked(true);
        break;
    default:
        radioAllowC->setChecked(true);
        break;
    }

    applyBtn->setEnabled(false);
}

bool OptionsDlg::testBtnStatus() const
{
    return (!urlEdit->text().isEmpty() &&
            !userEdit->text().isEmpty() &&
            !passwEdit->text().isEmpty() &&
            (m_jiraTest.isNull() || !m_jiraTest->hasPendingRequests()));
}

void OptionsDlg::restateControls()
{
    applyBtn->setEnabled(true);
    testBtn->setEnabled(testBtnStatus());
    spinActivity->setEnabled(checkActivity->isChecked());
    spinRefresh->setEnabled(checkRefresh->isChecked());
    spinReminder->setEnabled(checkReminder->isChecked());
    spinFailSafe->setEnabled(checkFailSafe->isChecked());
}

void OptionsDlg::applySettings()
{
    saveOptions();
}

void OptionsDlg::acceptChanges()
{
    if (applyBtn->isEnabled())
    {
        saveOptions();
        this->hide();
    }

    Preferences pref;
    pref.setValue(QString::fromUtf8(SIZE_KEY), size());
    pref.setValue(QString::fromUtf8(POS_KEY), pos());
    accept();
}

void OptionsDlg::cancelChanges()
{
    reject();
}

void OptionsDlg::testConnect()
{    
    m_jiraTest.reset(new JiraClient(urlEdit->text(), userEdit->text(), passwEdit->text()));
    connect(m_jiraTest.data(), SIGNAL(user(JiraUser)), this, SLOT(connectOk(JiraUser)));
    connect(m_jiraTest.data(), SIGNAL(error(Error, QString)), this, SLOT(connectError(Error, QString)));
    m_jiraTest->user(userEdit->text());
    testBtn->setEnabled(testBtnStatus());
}

void OptionsDlg::connectOk(const JiraUser&)
{
    testBtn->setEnabled(testBtnStatus());
    QMessageBox::information(this, tr("Jira"), tr("Jira connection is ok."));    
}

void OptionsDlg::connectError(Error, const QString& message)
{
    testBtn->setEnabled(testBtnStatus());
    QMessageBox::warning(this, tr("Jira"), tr("Jira connection failed: ") + message);
}

