#include "filterlistmodel.h"
#include "preferences/preferences.h"

FilterListModel::FilterListModel(QObject *parent) :
    QAbstractListModel(parent)
{    
}

void FilterListModel::mergeFilters(const QList<JiraFilter>& flist)
{
    QList<JiraFilter>::iterator itr = filters.begin();

    // cleanup
    while(itr != filters.end())
    {
        if (itr->id == JiraFilter::SELF_FILTER_ID || flist.contains(*itr))
        {
            ++itr;
        }
        else
        {
            beginRemoveRows(QModelIndex(), itr2pos(itr), itr2pos(itr));
            itr = filters.erase(itr);
            endRemoveRows();
        }
    }

    // merge
    foreach(JiraFilter jf, flist)
    {
        QList<JiraFilter>::iterator pos = qLowerBound(filters.begin(), filters.end(), jf);

        if (pos == filters.end() || *pos != jf)
        {
            beginInsertRows(QModelIndex(), itr2pos(pos), itr2pos(pos));
            filters.insert(pos, jf);
            endInsertRows();
        }
        else
        {
            if (!jf.equal(*pos))  // check content - optional
            {
                *pos = jf;
                dataChanged(createIndex(itr2pos(pos), 0), createIndex(itr2pos(pos), 0));
            }
        }
    }
}

int FilterListModel::itr2pos(const QList<JiraFilter>::const_iterator& itr) const
{
    return (itr - filters.begin());
}

int FilterListModel::rowCount(const QModelIndex& /*parent = QModelIndex()*/) const
{
    return filters.size();
}

QVariant FilterListModel::data(const QModelIndex & index, int role/* = Qt::DisplayRole*/) const
{
    if (!index.isValid())
        return QVariant();

    Q_ASSERT(index.row() < filters.size());
    if (role == Qt::DisplayRole)
        return filters.at(index.row()).name;

    if (role == JQLRole)
    {
        Preferences pref;
        QString jql = filters.at(index.row()).jql;
        if (filters.at(index.row()).id == JiraFilter::SELF_FILTER_ID)
            jql += pref.getUsername();

        return jql;
    }

    return QVariant();
}

void FilterListModel::save()
{
    Preferences pref;

    pref.beginGroup("FilterListModel");
    pref.beginWriteArray("Filters");

    for(int i = 1; i < filters.size(); i++)
    {
        pref.setArrayIndex(i - 1);
        pref.setFilterId(filters.at(i).id);
        pref.setFilterNameByUser(filters.at(i).name);
        pref.setFilterJql(filters.at(i).jql);
        pref.setFilterSelf(filters.at(i).self);
    }

    pref.endArray();
    pref.endGroup();
}

void FilterListModel::load()
{
    Preferences pref;
    pref.beginGroup("FilterListModel");
    int count = pref.beginReadArray("Filters");

    //filters.append(JiraFilter(JiraFilter::SELF_FILTER_ID, tr("Assigned to me"), "assignee="+pref.getUsername()));
    QList<JiraFilter> flist;
    flist << JiraFilter(JiraFilter::SELF_FILTER_ID, tr("Assigned to me"), "self", "assignee=");

    for(int i = 0; i < count; ++i)
    {
        pref.setArrayIndex(i);
        flist << JiraFilter(pref.getFilterId(), pref.getFilterNameByUser(), pref.getFilterSelf(), pref.getFilterJql());
    }

    mergeFilters(flist);

    pref.endArray();
}
