#ifndef __MAIN_WINDOW_H
#define __MAIN_WINDOW_H

#include <QMainWindow>
#include <QPointer>
#include <QSystemTrayIcon>
#include "ui_mainwindow.h"
#include "jiraclient.h"
#include "issuetablemodel.h"
#include "filterlistmodel.h"
#include "status_bar.h"
#include "idledetector/idle.h"

class QCloseEvent;
class QLineEdit;
class QAction;
class QMenu;
class QToolBar;
class QComboBox;
class QLabel;

class MainWindow : public QMainWindow, private Ui::MainWindow
{
    Q_OBJECT
public:
    MainWindow(QWidget* parent = 0);
    ~MainWindow();
private:
    QMenu*              trayIconMenu;
    QSystemTrayIcon*    trayIcon;
    QAction*            minimizeAction;
    QAction*            maximizeAction;
    QAction*            restoreAction;
    QAction*            quitAction;
    QToolBar*           filtersToolBar;
    QToolBar*           issuesToolBar;
    QComboBox*          filterCombo;
    QAction*            refreshFiltersAction;
    QLineEdit*          filterEdit;
    QAction*            filterAction;
    QAction*            preferencesAction;
    QAction*            questionAction;
    QAction*            registerAction;
    QAction*            refreshIssuesAction;
    QScopedPointer<JiraClient>          session;
    QScopedPointer<IssueTableModel>     imodel;
    QScopedPointer<IssuesSort>          smodel;
    QScopedPointer<FilterListModel>     fmodel;
    QScopedPointer<Psi::Idle>           idle;
    QMenu*              issuesMenu;
    QAction*            currentIssueAction;
    QAction*            startProgressAction;
    QAction*            stopProgressAction;
    QAction*            logWorkAction;
    QAction*            publishWorklogAction;
    QAction*            cancelWorklogAction;
    QTimer*             reminder;
    QTimer*             moneyPrayer;
    QTimer*             failsafe;
    qint32              overIdle;
    status_bar*         statusBar;
    void showPreferences(int page);
    void readSettings();
    QModelIndex s2i(const QModelIndex& index) const;    // sort model to issue model index conversion
    void restateIssuesToolBar();
    bool allFilled(const QModelIndexList& selected);
    bool anyLogged(const QModelIndexList& selected);
    QString status();
    void applyRegistration();
    void setWinTitle();
    void regNotification();
    bool regAvailable();
protected:
    void closeEvent(QCloseEvent *event);
    void showEvent(QShowEvent * event);
    bool event(QEvent * event);
private slots:
    void writeSettings();
    void filterChanged(const QString& text);
    void messageClicked();
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void optionsChanged();
    void minimize();
    void preferences();
    void processError(Error);
    void requestStarted(const JiraRequest&);
    void requestFinished(const JiraRequest&);
    void addIssue(); // only for testing purposes
    void updateIssue(const JiraIssue&);
    void mergeIssues(const QList<JiraIssue>&);
    void mergeFilters(const QList<JiraFilter>&);
    void worklogAdded(const JiraWorklog&);
    void issueChanged(const QString&);
    void displayHSMenu(const QPoint&);
    void issuesCtxMenu(const QPoint&);
    void startProgress();
    void startProgress(const QModelIndex& issueIndex);
    void stopProgress();
    void stopProgress(const QModelIndex& issueIndex);
    void toggleProgress(const QModelIndex&);
    void logWork();
    void publishWorklog();
    void cancelWorklog();
    void sortIssuesRequested(int, Qt::SortOrder);
    void idleHandler(int);
    void refreshFilters();
    void refreshIssues();
    void applyFilter();
    void filtersComboIndexChanged(int);
    void issuesSelectionChanged(const QItemSelection&, const QItemSelection&);
    void handleIssuesChanged(const QModelIndex&, const QModelIndex&);
    void showNotificationBaloon(QString title, QString msg) const;
    void showAbout();
    void showAboutReg();
    void remind();
    void prayMoney();
};

#endif

