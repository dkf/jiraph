#include <QDebug>
#include <QDateTime>
#include <QUrl>
#include <QStringList>

#include "jiraresource.h"
#include "utils.h"

const qint32 JiraFilter::SELF_FILTER_ID = 0;  //program defined filter
const qint32 JiraFilter::UNDEFINED_FILTER_ID = -1;

#define JSET(jobject, jfield) extract(trg.jfield, jobject, #jfield)

const qlonglong JiraIssue::UNDEFINED_ID = -1;

JiraIssue::JiraIssue() : id(UNDEFINED_ID)
{
    aggregatetimeestimate = 0;
    aggregatetimeoriginalestimate = 0;
    aggregatetimespent = 0;

    timespent = 0;
    timeestimate = 0;
    timeoriginalestimate = 0;
    workratio = 0;
}

JiraUser json2user(const QtJson::JsonObject& j)
{
    JiraUser trg;
    JSET(j, name);
    JSET(j, emailAddress);
    JSET(j, displayName);
    return trg;
}

JiraIssue json2issue(const QtJson::JsonObject& j)
{
    JiraIssue trg;
    JSET(j, id);
    JSET(j, key);
    JSET(j, self);

    JSET(j["fields"].toMap(), summary);
    JSET(j["fields"].toMap(), description);

    JSET(j["fields"].toMap(), aggregatetimeestimate);
    JSET(j["fields"].toMap(), aggregatetimeoriginalestimate);
    JSET(j["fields"].toMap(), aggregatetimespent);

    JSET(j["fields"].toMap(), timespent);
    JSET(j["fields"].toMap(), timeestimate);
    JSET(j["fields"].toMap(), timeoriginalestimate);
    JSET(j["fields"].toMap(), workratio);

    trg.created = QDateTime::fromString(extract<QString>(j["fields"].toMap(), "created"), Qt::ISODate);
    trg.updated = QDateTime::fromString(extract<QString>(j["fields"].toMap(), "updated"), Qt::ISODate);
    trg.duedate = QDateTime::fromString(extract<QString>(j["fields"].toMap(), "duedate"), Qt::ISODate);
    trg.lastViewed = QDateTime::fromString(extract<QString>(j["fields"].toMap(), "lastViewed"), Qt::ISODate);

    extract(trg.status.first, j["fields"].toMap().value("status").toMap(), "id");
    extract(trg.status.second, j["fields"].toMap().value("status").toMap(), "name");

    extract(trg.priority.first, j["fields"].toMap().value("priority").toMap(), "id");
    extract(trg.priority.second, j["fields"].toMap().value("priority").toMap(), "name");

    extract(trg.progress.first, j["fields"].toMap().value("progress").toMap(), "percent");
    extract(trg.progress.second.first, j["fields"].toMap().value("progress").toMap(), "total");
    extract(trg.progress.second.second, j["fields"].toMap().value("progress").toMap(), "progress");

    extract(trg.assignee.self, j["fields"].toMap().value("assignee").toMap(), "self");
    extract(trg.assignee.email, j["fields"].toMap().value("assignee").toMap(), "emailAddress");
    extract(trg.assignee.displayName, j["fields"].toMap().value("assignee").toMap(), "displayName");
    extract(trg.assignee.name, j["fields"].toMap().value("assignee").toMap(), "name");

    extract(trg.reporter.self, j["fields"].toMap().value("reporter").toMap(), "self");
    extract(trg.reporter.email, j["fields"].toMap().value("reporter").toMap(), "emailAddress");
    extract(trg.reporter.displayName, j["fields"].toMap().value("reporter").toMap(), "displayName");
    extract(trg.reporter.name, j["fields"].toMap().value("reporter").toMap(), "name");

    extract(trg.issueType.self, j["fields"].toMap().value("issuetype").toMap(), "self");
    extract(trg.issueType.id, j["fields"].toMap().value("issuetype").toMap(), "id");
    extract(trg.issueType.description, j["fields"].toMap().value("issuetype").toMap(), "description");
    extract(trg.issueType.name, j["fields"].toMap().value("issuetype").toMap(), "name");
    extract(trg.issueType.subtask, j["fields"].toMap().value("issuetype").toMap(), "subtask");

    return trg;
}

QList<JiraIssue> json2issueList(const QVariant& json)
{
    QList<JiraIssue> result;
    foreach(const QVariant& item, json.toMap()["issues"].toList())
    {
        result << json2issue(item.toMap());
    }

    return result;
}

JiraFilter json2filter(const QtJson::JsonObject& json)
{
    JiraFilter trg;
    JSET(json, id);
    JSET(json, name);
    JSET(json, self);
    JSET(json, jql);
    JSET(json, viewUrl);
    JSET(json, searchUrl);
    return trg;
}

QList<JiraFilter> json2filterList(const QVariant& json)
{
    QList<JiraFilter> result;
    foreach(const QVariant& item, json.toList())
    {
        result << json2filter(item.toMap());
    }

    return result;
}

QtJson::JsonObject pendingWorklog2json(const JiraPendingWorklog& pw)
{
    QtJson::JsonObject json;
    if (!pw.issueIdOrKey.isEmpty())
        json.insert("issueIdOrKey", pw.issueIdOrKey);
    if (!pw.timeSpent.isEmpty())
        json.insert("timeSpent", pw.timeSpent);
    if (pw.timeSpentSeconds)
        json.insert("timeSpentSeconds", pw.timeSpentSeconds);
    if (!pw.comment.isEmpty())
        json.insert("comment", pw.comment);
    return json;
}

JiraFilter::JiraFilter() : id(JiraFilter::UNDEFINED_FILTER_ID) {}

JiraFilter::JiraFilter(qint32 _id, const QString& _name, const QString& _self, const QString& _jql):
    id(_id), name(_name), self(_self), jql(_jql) {}

JiraWorklog json2worklog(const QtJson::JsonObject& json)
{
    JiraWorklog trg;
    JSET(json, id);
    JSET(json, self);
    JSET(json, comment);
    JSET(json, timeSpent);
    JSET(json, timeSpentSeconds);
    return trg;
}

qlonglong JiraWorklog::issueId() const
{
    return utils::url2issueid(self);
}
