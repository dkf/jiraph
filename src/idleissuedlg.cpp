#include "idleissuedlg.h"
#include "ui_idleissuedlg.h"
#include "utils.h"
#include <QTimer>
#include <QPushButton>

//-----------------------------------------------------------
IdleIssueDlg::IdleIssueDlg(int& idle, QWidget* parent):
    QDialog(parent), ui(new Ui_IdleIssueDlg), current_idle(idle)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Dialog | Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::WindowStaysOnTopHint);
    connect(ui->issuesCombo, SIGNAL(editTextChanged(const QString&)), SLOT(onTextChanged(const QString&)));
    Q_ASSERT(ui->buttonBox->button(QDialogButtonBox::Yes));
    ui->buttonBox->button(QDialogButtonBox::Yes)->setDisabled(true);
    setWindowTitle(QApplication::applicationName());
}
//-----------------------------------------------------------
IdleIssueDlg::~IdleIssueDlg()
{
   delete ui;
}
//-----------------------------------------------------------
void IdleIssueDlg::onTextChanged(const QString &text)
{
    Q_ASSERT(ui->buttonBox->button(QDialogButtonBox::Yes));
    ui->buttonBox->button(QDialogButtonBox::Yes)->setDisabled(text.isEmpty());
}

//-----------------------------------------------------------
void IdleIssueDlg::onTimer()
{
    current_idle += 1;
    updateIdle();
}

//-----------------------------------------------------------
QString IdleIssueDlg::getIssueKey(const QStringList &issues, const QString &currentIssue)
{
    updateIdle();
    connect(&tm, SIGNAL(timeout()), this, SLOT(onTimer()));
    tm.start(1000);
    ui->issuesCombo->clear();
    ui->issuesCombo->addItems(issues);
    ui->issuesCombo->setEditText(currentIssue);
    return (QDialog::exec() == QDialog::Accepted)? ui->issuesCombo->currentText() : QString("");
}

void IdleIssueDlg::updateIdle()
{
    ui->msgLbl->setText(tr("System is idle %1").arg(utils::userFriendlyHours(current_idle)));
}

