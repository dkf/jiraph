#include "jiraclient.h"

JiraRequest::JiraRequest(Type type, const QString& desctiption,
                         HttpRequest::Method method, const QUrl& endpoint,
                         const Headers& headers, const Params& params, const QByteArray& data):
    HttpRequest(method, endpoint, headers, params, data), m_type(type), m_description(desctiption)
{
}

QString JiraRequest::issueIdOrKey() const
{
    return m_endpoint.path().split("/").at(5); // jira/rest/api/2/issue/10010/worklog/10000 -> 10010
}

JiraClient::JiraClient(
    const QString& server, const QString& username, const QString& password, int api_version)
{
    reset(server, username, password, api_version);

    connect(&m_httpConnector, SIGNAL(progress(qint64,qint64)), this, SIGNAL(progress(qint64,qint64)));
    connect(&m_httpConnector, SIGNAL(requestFinished(QByteArray)), this, SLOT(requestFinished(QByteArray)));
    connect(&m_httpConnector, SIGNAL(requestError(Error)), this, SLOT(requestError(Error)));
}

void JiraClient::reset(const QString& server, const QString& username, const QString& password, int api_version)
{
    m_baseURL = QString("%1/rest/api/%2/").arg(server, QString::number(api_version));

    m_headers.clear();
    m_headers << qMakePair<QByteArray,QByteArray>(
        "Authorization", QByteArray("Basic ") + QString("%1:%2").arg(username).arg(password).toAscii().toBase64());
    m_headers << qMakePair<QByteArray,QByteArray>("Content-Type", "application/json;charset=UTF-8");
}

void JiraClient::user(const QString& username)
{
    HttpRequest::Params params;
    params << qMakePair(QString("username"), username);
    sendRequest(JiraRequest(JiraRequest::USER, "Requesting user info",
                            HttpRequest::GET, getURL("user"), m_headers, params));
}

void JiraClient::searchIssues(
    const QString& jql, int startAt, int maxResults /*, const QString& fields , const QString& expand*/)
{
    HttpRequest::Params params;
    params << qMakePair(QString("jql"), jql);
    params << qMakePair(QString("startAt"), QString::number(startAt));
    params << qMakePair(QString("maxResults"), QString::number(maxResults));
    //params << qMakePair(QString("fields"), fields);
    //params << qMakePair(QString("expand"), expand);
    sendRequest(JiraRequest(JiraRequest::ISSUE_LIST, "Requesting issue list",
                            HttpRequest::GET, getURL("search"), m_headers, params));
}

void JiraClient::issue(const QString& issueIdOrKey /*, const QString& fields = "*all", const QString& expand = ""*/)
{
    HttpRequest::Params params;
    //params << qMakePair(QString("fields"), fields);
    //params << qMakePair(QString("expand"), expand);
    sendRequest(JiraRequest(JiraRequest::ISSUE, "Requesting issue info",
                            HttpRequest::GET, getURL(QString("issue/%1").arg(issueIdOrKey)), m_headers, params));
}

void JiraClient::favouriteFilters(/*const QString& expand*/)
{
    HttpRequest::Params params;
    //params << qMakePair(QString("expand"), expand);
    sendRequest(JiraRequest(JiraRequest::FILTER_LIST, "Requesting favourite filters",
                            HttpRequest::GET, getURL("filter/favourite"), m_headers, params));
}

void JiraClient::addWorklog(const QString& issueIdOrKey, const QString& timeSpent,
                            qint32 timeSpentSeconds, const QString& comment,
                            const QString& adjustEstimate, const QString& newEstimate,
                            const QString& reduceBy)
{
    HttpRequest::Params params;
    if (!adjustEstimate.isEmpty())
        params << qMakePair(QString("adjustEstimate"), adjustEstimate);
    if (!newEstimate.isEmpty())
        params << qMakePair(QString("newEstimate"), newEstimate);
    if (!reduceBy.isEmpty())
        params << qMakePair(QString("reduceBy"), reduceBy);

    QByteArray data = QtJson::serialize(
        pendingWorklog2json(JiraPendingWorklog("", timeSpent, timeSpentSeconds, comment)));
    sendRequest(JiraRequest(JiraRequest::ADD_WORKLOG, "Adding worklog",
                            HttpRequest::POST, getURL(QString("issue/%1/worklog").arg(issueIdOrKey)),
                            m_headers, params, data));
}

void JiraClient::addTransition(const QString& issueIdOrKey, qlonglong transitionId)
{
    HttpRequest::Params params;
    QtJson::JsonObject json;
    QtJson::JsonObject transition;
    transition["id"] = transitionId;
    json["transition"] = transition;
    QByteArray data = QtJson::serialize(json);
    sendRequest(JiraRequest(JiraRequest::ADD_TRANSITION, "Changing issue state",
                            HttpRequest::POST, getURL(QString("issue/%1/transitions").arg(issueIdOrKey)),
                            m_headers, params, data));
}

void JiraClient::requestFinished(const QByteArray& data)
{
    bool ok;
    QVariant json = QtJson::parse(QString::fromUtf8(data.constData()), ok);

    JiraRequest request = pendingRequest();
    sendNextRequest();

    if (ok)
    {
        switch(request.type())
        {
        case JiraRequest::USER:
            emit user(json2user(json.toMap()));
            break;
        case JiraRequest::ISSUE:
            emit issue(json2issue(json.toMap()));
            break;
        case JiraRequest::ISSUE_LIST:
            emit issueList(json2issueList(json));
            break;
        case JiraRequest::FILTER_LIST:
            emit filterList(json2filterList(json));
            break;
        case JiraRequest::ADD_WORKLOG:
            emit worklogAdded(json2worklog(json.toMap()));
            break;
        case JiraRequest::ADD_TRANSITION:
            emit transitionAdded(request.issueIdOrKey());
            break;
        case JiraRequest::UNKNOWN:
            emit error(Error(Error::UNKNOWN_RESULT, "Unknown result type"));
            break;
        }
    }
    else
    {
        emit error(Error(Error::JSON_ERROR, "JSON parsing error"));
    }
}

void JiraClient::requestError(Error err)
{
    JiraRequest request = pendingRequest();
    sendNextRequest();

    if (request.type() == JiraRequest::ADD_TRANSITION && err.type == Error::CONTENT_ERROR) {
        err.type = Error::BAD_TRANSITION;
        err.message = QString("Can't change status for issue %1").arg(request.issueIdOrKey());
        emit error(err);
    }
    else
        emit error(err);
}

QUrl JiraClient::getURL(const QString& path) const
{
    QUrl url(m_baseURL + path);
    //url.setScheme("https");
    return url;
}

void JiraClient::sendRequest(const JiraRequest& request)
{
    if (m_pendingRequests.isEmpty())
        sendRequestNow(request);

    m_pendingRequests.enqueue(request);
}

void JiraClient::sendNextRequest()
{
    if (!m_pendingRequests.isEmpty())
        emit requestFinished(m_pendingRequests.dequeue());

    if (!m_pendingRequests.isEmpty())
        sendRequestNow(m_pendingRequests.head());
}

void JiraClient::sendRequestNow(const JiraRequest& request)
{
    m_httpConnector.request(request);
    emit requestStarted(request);
}

JiraRequest JiraClient::pendingRequest() const
{
    return m_pendingRequests.isEmpty() ? JiraRequest() : m_pendingRequests.head();
}
