#include <QNetworkAccessManager>
#include <QDebug>

#include "httpconnector.h"
#include "httprequest.h"

HttpConnector::HttpConnector()
{
    m_manager = new QNetworkAccessManager();

    if (!QSslSocket::supportsSsl())
        qDebug() << "SSL is not available!";
    else
        qDebug() << "SSL ready";

    connect(m_manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(finished(QNetworkReply*)));
}

HttpConnector::~HttpConnector()
{
    delete m_manager;
}

QByteArray HttpConnector::request(const HttpRequest& request)
{
    m_replyData.clear();

    qDebug() << "Request: " << request.endpoint();

    QNetworkRequest networkRequest(request.endpoint());
    HttpRequest::Headers httpHeaders = request.headers();

    qDebug() << "Headers: ";
    typedef QPair<QByteArray,QByteArray> QPairByteArray;
    foreach(const QPairByteArray& hp, httpHeaders)
    {
        qDebug() << hp.first << ": " << hp.second;
        networkRequest.setRawHeader(hp.first, hp.second);
    }

    HttpRequest::Method httpMethod = request.method();
    const HttpRequest::Params& params = request.params();
    const QByteArray& data = request.data();
    switch(httpMethod)
    {
    case HttpRequest::GET:
        qDebug() << "GET";
        qDebug() << "Params: " << params;
        m_reply = m_manager->get(networkRequest);
        break;
    case HttpRequest::POST:
        qDebug() << "POST";
        qDebug() << "Params: " << params;
        qDebug() << "Data: " << data;
        m_reply = m_manager->post(networkRequest, data);
        break;
    case HttpRequest::PUT:
        qDebug() << "PUT";
        qDebug() << "Params: " << params;
        qDebug() << "Data: " << data;
        m_reply = m_manager->put(networkRequest, data);
        break;
    case HttpRequest::HEAD:
        qDebug() << "PUT";
        m_reply = m_manager->head(networkRequest);
        break;
    case HttpRequest::DELETE:
        qDebug() << "DELETE";
        m_reply = m_manager->deleteResource(networkRequest);
        break;
    }

    connect(m_reply, SIGNAL(downloadProgress(qint64,qint64)), this, SIGNAL(progress(qint64,qint64)));
    connect(m_reply, SIGNAL(sslErrors(QList<QSslError>)), this, SLOT(sslErrors(QList<QSslError>)));

    return m_replyData;
}

void HttpConnector::finished(QNetworkReply* netReply)
{
    const QList<QNetworkReply::RawHeaderPair> headerList = netReply->rawHeaderPairs();

    qDebug() << "HTTP Reply header:";
    foreach(const QNetworkReply::RawHeaderPair& hp, headerList)
        qDebug() << hp.first << ": " << hp.second;

    emit headersRetrieved(headerList);
    netReply->deleteLater();

    m_replyData = m_reply->readAll();
    qDebug() << "HTTP Reply body: ";
    qDebug() << m_replyData;

    if (m_reply->error() == QNetworkReply::NoError)
        emit requestFinished(m_replyData);
    else
        handleError(m_reply->error());
}

void HttpConnector::handleError(QNetworkReply::NetworkError networkError)
{
    Error error(networkError, Error::NO_ERROR);

    switch(networkError)
    {
    case QNetworkReply::NoError:
        break;
    case QNetworkReply::ConnectionRefusedError:
    case QNetworkReply::RemoteHostClosedError:
    case QNetworkReply::HostNotFoundError:
    case QNetworkReply::TimeoutError:
    case QNetworkReply::OperationCanceledError:
    case QNetworkReply::SslHandshakeFailedError:
    case QNetworkReply::TemporaryNetworkFailureError:
    case QNetworkReply::UnknownNetworkError:
        error.type = Error::NETWORK_ERROR;
        error.message = "Network layer error";
        break;
    case QNetworkReply::ProxyConnectionRefusedError:
    case QNetworkReply::ProxyConnectionClosedError:
    case QNetworkReply::ProxyNotFoundError:
    case QNetworkReply::ProxyTimeoutError:
    case QNetworkReply::ProxyAuthenticationRequiredError:
    case QNetworkReply::UnknownProxyError:
        error.type = Error::PROXY_ERROR;
        error.message = "Proxy error";
        break;
    case QNetworkReply::AuthenticationRequiredError:
        error.type = Error::AUTH_ERROR;
        error.message = "Authentication error";
        break;
    case QNetworkReply::ContentAccessDenied:
    case QNetworkReply::ContentOperationNotPermittedError:
        error.type = Error::PERMISSION_ERROR;
        error.message = "Operation not permitted";
        break;
    case QNetworkReply::ContentNotFoundError:
    case QNetworkReply::ContentReSendError:
    case QNetworkReply::UnknownContentError:
        error.type = Error::CONTENT_ERROR;
        error.message = "Content error";
        break;
    case QNetworkReply::ProtocolUnknownError:
    case QNetworkReply::ProtocolInvalidOperationError:
    case QNetworkReply::ProtocolFailure:
        error.type = Error::PROTOCOL_ERROR;
        error.message = "Protocol error";
        break;
    }

    qDebug() << "HTTP error: " << error.message;
    emit requestError(error);
}

void HttpConnector::sslErrors(const QList<QSslError>& errors)
{
    qDebug() << "Ignored SSL errors:";
    foreach(const QSslError& se, errors)
        qDebug() << se;

    m_reply->ignoreSslErrors();
}
