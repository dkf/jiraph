#include <QBrush>
#include "issuetablemodel.h"
#include "utils.h"

AnnotatedJiraIssue AnnotatedJiraIssue::fromJiraIssue(const JiraIssue& ji)
{
    return AnnotatedJiraIssue(ji);
}

AnnotatedJiraIssue::AnnotatedJiraIssue(const JiraIssue& j) : issue(j), worklogText(QString("")), worklogAmount(0){}

AnnotatedJiraIssue::AnnotatedJiraIssue(const Preferences& pref)
{
    worklogAmount   = pref.getWorkLog();
    worklogText     = pref.getWorkLogText();

    issue.id        = pref.getId();
    issue.key       = pref.getKey();
    issue.self      = pref.getSelf();
    issue.summary   = pref.getSummary();
    issue.description  = pref.getDescription();
    issue.aggregatetimeestimate = pref.getAggregatetimeestimate();
    issue.aggregatetimeoriginalestimate = pref.getAggregatetimeoriginalestimate();
    issue.aggregatetimespent    = pref.getAggregatetimespent();
    issue.timespent = pref.getTimespent();
    issue.timeestimate  = pref.getTimeestimate();
    issue.timeoriginalestimate = pref.getTimeoriginalestimate();
    issue.workratio = pref.getWorkratio();
    issue.created = pref.getCreated();
    issue.updated = pref.getUpdated();
    issue.duedate = pref.getDuedate();
    issue.lastViewed = pref.getLastViewed();
    issue.status = qMakePair(pref.getStatus_first(), pref.getStatus_second());
    issue.priority = qMakePair(pref.getPriority_first(), pref.getPriority_second());
    issue.progress = qMakePair(static_cast<qint32>(pref.getProgress_first()),
                               qMakePair(static_cast<qint32>(pref.getProgress_second()),
                                         static_cast<qint32>(pref.getProgress_third())));

    issue.assignee.self = pref.getAssignee_self();
    issue.assignee.name = pref.getAssignee_name();
    issue.assignee.email = pref.getAssignee_email();
    issue.assignee.displayName = pref.getAssignee_dname();

    issue.reporter.self = pref.getReporter_self();
    issue.reporter.name = pref.getReporter_name();
    issue.reporter.email = pref.getReporter_email();
    issue.reporter.displayName = pref.getReporter_dname();

    issue.issueType.self    = pref.getIssueType_self();
    issue.issueType.id      = pref.getIssueType_id();
    issue.issueType.name    = pref.getIssueType_name();
    issue.issueType.description = pref.getIssueType_descr();
    issue.issueType.subtask = pref.getIssueType_subtask();
}

void AnnotatedJiraIssue::save(Preferences& pref)
{
    pref.setWorkLog(worklogAmount);
    pref.setWorkLogText(worklogText);

    pref.setId(issue.id);
    pref.setKey(issue.key);
    pref.setSelf(issue.self);
    pref.setSummary(issue.summary);
    pref.setDescription(issue.description);
    pref.setAggregatetimeestimate(issue.aggregatetimeestimate);
    pref.setAggregatetimeoriginalestimate(issue.aggregatetimeoriginalestimate);
    pref.setAggregatetimespent(issue.aggregatetimespent);
    pref.setTimespent(issue.timespent);
    pref.setTimeestimate(issue.timeestimate);
    pref.setTimeoriginalestimate(issue.timeoriginalestimate);
    pref.setWorkratio(issue.workratio);
    pref.setCreated(issue.created);
    pref.setUpdated(issue.updated);
    pref.setDuedate(issue.duedate);
    pref.setLastViewed(issue.lastViewed);
    pref.setStatus_first(issue.status.first);
    pref.setStatus_second(issue.status.second);

    pref.setPriority_first(issue.priority.first);
    pref.setPriority_second(issue.priority.second);

    pref.setProgress_first(issue.progress.first);
    pref.setProgress_second(issue.progress.second.first);
    pref.setProgress_third(issue.progress.second.second);

    pref.setAssignee_self(issue.assignee.self);
    pref.setAssignee_name(issue.assignee.name);
    pref.setAssignee_email(issue.assignee.email);
    pref.setAssignee_dname(issue.assignee.displayName);

    pref.setReporter_self(issue.reporter.self);
    pref.setReporter_name(issue.reporter.name);
    pref.setReporter_email(issue.reporter.email);
    pref.setReporter_dname(issue.reporter.displayName);

    pref.setIssueType_self(issue.issueType.self);
    pref.setIssueType_id(issue.issueType.id);
    pref.setIssueType_name(issue.issueType.name);
    pref.setIssueType_descr(issue.issueType.description);
    pref.setIssueType_subtask(issue.issueType.subtask);
}

IssueTableModel::IssueTableModel() : current_index(-1), current_ticket(-1)
{
    connect(&timer, SIGNAL(timeout()), this, SLOT(updateWorkLog()));
    timer.start(1000);
}

void IssueTableModel::updateIssue(const AnnotatedJiraIssue& aissue)
{
    int index = ticketId2index(aissue.issue.id);
    if (index != -1)
    {
        issues[index].issue = aissue.issue;
        dataChanged(createIndex(index, 0), createIndex(index, columnCount() - 1));
    }
}

void IssueTableModel::mergeIssues(const QList<AnnotatedJiraIssue>& ilist)
{
    QList<AnnotatedJiraIssue>::iterator itr = issues.begin();

    // cleanup
    while(itr != issues.end())
    {
        if (itr->persistent() || ilist.contains(*itr))
        {
            ++itr;
        }
        else
        {
            beginRemoveRows(QModelIndex(), itr2pos(itr), itr2pos(itr));
            itr = issues.erase(itr);
            endRemoveRows();
        }
    }

    // merge
    foreach(AnnotatedJiraIssue aji, ilist)
    {
        QList<AnnotatedJiraIssue>::iterator pos = qLowerBound(issues.begin(), issues.end(), aji);

        if (pos == issues.end() || *pos != aji)  // insert into list when element out of range or in this position different id
        {
            beginInsertRows(QModelIndex(), itr2pos(pos), itr2pos(pos));
            issues.insert(pos, aji);
            endInsertRows();
        }
        else
        {
            if (!aji.issue.equal(pos->issue))  // check content equality
            {
                pos->issue = aji.issue;
                dataChanged(createIndex(itr2pos(pos), 0), createIndex(itr2pos(pos), columnCount() - 1));
            }
        }
    }


    if (current_index != -1)
    {
        // we have worklog in progress
        current_index = ticketId2index(current_ticket);
        Q_ASSERT(current_index != -1); // this issue must been in issues list
        Q_ASSERT(current_index < issues.size());
    }
}


int IssueTableModel::rowCount(const QModelIndex&) const  {return issues.size();}
int IssueTableModel::columnCount(const QModelIndex&) const { return DC_END; }

QVariant IssueTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation);

    if (role != Qt::DisplayRole)
        return QVariant();

    switch (section)
    {
        case DC_ID: return tr("Ticket id");
        case DC_KEY: return tr("Key");
        case DC_SELF: return tr("Self");
        case DC_SUMMARY: return tr("Summary");
        case DC_DESCRIPTION: return tr("Description");
        case DC_STATUS: return tr("Status");
        case DC_PRIOPRITY: return tr("Priority");
        case DC_TIMESPENT: return tr("Time spent");
        case DC_TIMEESTIMATE: return tr("Time estimate");
        case DC_TIMEORIGINALESTIMATE: return tr("Time original estimate");
        case DC_CREATED: return tr("Created");
        case DC_UPDATED: return tr("Updated");
        case DC_DUEDATE: return tr("Duedate");
        case DC_LASTVIEWED: return tr("Last viewed");
        case DC_WORKLOG: return tr("Work log");
        case DC_WORKLOG_TEXT: return tr("Work log comment");
        case DC_ASSIGNEE_SELF: return tr("Assignee self");
        case DC_ASSIGNEE_NAME: return tr("Assignee name");
        case DC_ASSIGNEE_EMAIL: return tr("Assignee email");
        case DC_ASSIGNEE_DISPLAY_NAME: return tr("Assignee display name");
        case DC_REPORTER_SELF: return tr("Reporter self");
        case DC_REPORTER_NAME: return tr("Reporter name");
        case DC_REPORTER_EMAIL: return tr("Reporter email");
        case DC_REPORTER_DISPLAY_NAME:return tr("Reporter display name");
        case DC_ISSUE_TYPE_NAME: return tr("Issue type name");
        case DC_ISSUE_TYPE_SUBTASK: return tr("Subtask");
        default: return QVariant();
    }    
}

QVariant IssueTableModel::data(const QModelIndex & index, int role /*= Qt::DisplayRole*/) const
{
    if (!index.isValid())
        return QVariant();

    switch (role)
    {

    case SortRole:
        switch (index.column())
        {
        case DC_ID: return id(index);
        case DC_KEY: return key(index);
        case DC_SELF: return self(index);
        case DC_SUMMARY: return summary(index);
        case DC_DESCRIPTION: return description(index);
        case DC_STATUS: return status(index);
        case DC_PRIOPRITY: return priority(index);
        case DC_TIMESPENT: return timespent(index);
        case DC_TIMEESTIMATE: return timestimate(index);
        case DC_TIMEORIGINALESTIMATE: return timeoriginalestimate(index);
        case DC_CREATED: return created(index);
        case DC_UPDATED: return updated(index);
        case DC_DUEDATE: return duedate(index);
        case DC_LASTVIEWED: return lastviewed(index);
        case DC_WORKLOG: return worklog(index);
        case DC_WORKLOG_TEXT: return worklogText(index);
        case DC_ASSIGNEE_SELF: return assignee(index).self;
        case DC_ASSIGNEE_NAME: return assignee(index).name;
        case DC_ASSIGNEE_EMAIL: return assignee(index).email;
        case DC_ASSIGNEE_DISPLAY_NAME: return assignee(index).displayName;
        case DC_REPORTER_SELF: return reporter(index).self;
        case DC_REPORTER_NAME: return reporter(index).name;
        case DC_REPORTER_EMAIL: return reporter(index).email;
        case DC_REPORTER_DISPLAY_NAME: return reporter(index).displayName;
        case DC_ISSUE_TYPE_NAME: return itype(index).name;
        case DC_ISSUE_TYPE_SUBTASK: return itype(index).subtask;
            default:
                qWarning("data: invalid display value column %d", index.column());
            break;
        }
        break;
    case Qt::CheckStateRole:
        {
            //
        }
    break;
    case Qt::EditRole:
    case Qt::DisplayRole:
        switch (index.column())
        {
        case DC_ID: return id(index);
        case DC_KEY: return key(index);
        case DC_SELF: return self(index);
        case DC_SUMMARY: return summary(index);
        case DC_DESCRIPTION: return description(index);
        case DC_STATUS: return status(index);
        case DC_PRIOPRITY: return priority(index);
        case DC_TIMESPENT: return utils::userFriendlyDuration(timespent(index));
        case DC_TIMEESTIMATE: return utils::userFriendlyDuration(timestimate(index));
        case DC_TIMEORIGINALESTIMATE: return utils::userFriendlyDuration(timeoriginalestimate(index));
        case DC_CREATED: return created(index);
        case DC_UPDATED: return updated(index);
        case DC_DUEDATE: return duedate(index);
        case DC_LASTVIEWED: return lastviewed(index);
        case DC_WORKLOG: return utils::userFriendlyDuration(worklog(index));
        case DC_WORKLOG_TEXT: return worklogText(index);
        case DC_ASSIGNEE_SELF: return assignee(index).self;
        case DC_ASSIGNEE_NAME: return assignee(index).name;
        case DC_ASSIGNEE_EMAIL: return assignee(index).email;
        case DC_ASSIGNEE_DISPLAY_NAME: return assignee(index).displayName;
        case DC_REPORTER_SELF: return reporter(index).self;
        case DC_REPORTER_NAME: return reporter(index).name;
        case DC_REPORTER_EMAIL: return reporter(index).email;
        case DC_REPORTER_DISPLAY_NAME: return reporter(index).displayName;
        case DC_ISSUE_TYPE_NAME: return itype(index).name;
        case DC_ISSUE_TYPE_SUBTASK: return itype(index).subtask;
            default:
                qWarning("data: invalid display value column %d", index.column());
            break;
        }
        break;
    case Qt::DecorationRole:
        break;
    case Qt::TextAlignmentRole:
        break;
    case Qt::FontRole:
        break;
    case Qt::BackgroundRole:
        if (current_index == index.row())
        {
            return QBrush(Qt::lightGray);
        }
        break;
    default:
    ;
    }

    return QVariant();
}

Qt::ItemFlags IssueTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return Qt::ItemIsEnabled;

    if (index.column() == DC_WORKLOG_TEXT)
    {
         return QAbstractItemModel::flags(index) | Qt::ItemIsEditable;
    }

    return QAbstractItemModel::flags(index);
}

bool IssueTableModel::setData(const QModelIndex &index, const QVariant &value,  int role /*= Qt::EditRole*/)
{
    if (index.isValid() && role == Qt::EditRole)
    {
        Q_ASSERT(index.row() < issues.size());
        issues[(index.row())].worklogText = value.toString();
        emit dataChanged(index, index);
        return true;
    }
    return false;
}


qlonglong IssueTableModel::id(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.id;
}

QString IssueTableModel::key(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.key;
}

QString IssueTableModel::self(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.self;
}

QString IssueTableModel::summary(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.summary;
}

QString IssueTableModel::description(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.description;
}

QString IssueTableModel::status(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.status.second;
}

QString IssueTableModel::priority(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.priority.second;
}

qint32 IssueTableModel::timespent(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.timespent;
}

qint32 IssueTableModel::timestimate(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.timeestimate;
}

QString IssueTableModel::worklogText(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).worklogText;
}

const Persona& IssueTableModel::assignee(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.assignee;
}

const Persona& IssueTableModel::reporter(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.reporter;
}

const IssueType& IssueTableModel::itype(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.issueType;
}

qint32 IssueTableModel::timeoriginalestimate(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.timeoriginalestimate;
}

QDateTime IssueTableModel::created(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.created;
}

QDateTime IssueTableModel::updated(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.updated;
}

QDateTime IssueTableModel::duedate(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.duedate;
}

QDateTime IssueTableModel::lastviewed(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).issue.lastViewed;
}

qint32 IssueTableModel::worklog(const QModelIndex& indx) const
{
    Q_ASSERT(indx.row() < issues.size());
    return issues.at(indx.row()).worklogAmount;
}

int IssueTableModel::itr2pos(const QList<AnnotatedJiraIssue>::const_iterator& itr) const
{
    return (itr - issues.begin());
}

int IssueTableModel::ticketId2index(qlonglong id) const
{
    QList<AnnotatedJiraIssue>::const_iterator itr = qFind(issues.begin(), issues.end(), id);
    if (itr!=issues.end())
        return itr2pos(itr);
    return -1;
}

bool IssueTableModel::worklogged(const QModelIndex& indx) const
{
    return (worklog(indx) != 0);
}

bool IssueTableModel::currentProgress(const QModelIndex& index) const
{
    return (current_index == index.row());
}

QModelIndex IssueTableModel::currentProgress() const
{
    return createIndex(current_index, DC_WORKLOG);
}

void IssueTableModel::updateCurrentWorklog(qint32 amount)
{
    if (current_index != -1)
    {
        Q_ASSERT(current_index < issues.size());
        issues[current_index].worklogAmount += amount;
        emit dataChanged(createIndex(current_index, DC_WORKLOG), createIndex(current_index, DC_WORKLOG));
    }
}

void IssueTableModel::startWorkLog(const QModelIndex& indx)
{
    if (!indx.isValid())
        return;

    if (current_index != -1)
    {
        Q_ASSERT(current_index < issues.size());
    }

    current_index = indx.row();
    current_ticket = id(indx);
    emit dataChanged(createIndex(current_index, DC_WORKLOG), createIndex(current_index, DC_WORKLOG));
}

void IssueTableModel::stopWorkLog(const QModelIndex& indx)
{
    if (!indx.isValid())
        return;

    Q_ASSERT(current_index != -1);
    Q_ASSERT(current_ticket != -1);
    Q_ASSERT(indx.row() == current_index);

    current_index = -1;
    current_ticket = -1;
    emit dataChanged(createIndex(current_index, DC_WORKLOG), createIndex(current_index, DC_WORKLOG));
}

void IssueTableModel::cancelWorkLog(const QModelIndex& index)
{
    if (index.isValid())
    {
        issues[(index.row())].worklogAmount = 0;
        emit dataChanged(createIndex(index.row(), DC_WORKLOG), createIndex(index.row(), DC_WORKLOG));
    }
}

void IssueTableModel::updateWorkLog()
{
    updateCurrentWorklog(1);
}

void IssueTableModel::logWork(qlonglong issueId, qint32 timeSpentSeconds)
{
    int index = ticketId2index(issueId);
    Q_ASSERT(index != -1);
    issues[index].worklogAmount -= std::min(issues[index].worklogAmount, timeSpentSeconds);
    issues[index].worklogAmount = 0;
    issues[index].issue.timespent += timeSpentSeconds;
    emit dataChanged(createIndex(index, DC_WORKLOG), createIndex(index, DC_WORKLOG));
    emit dataChanged(createIndex(index, DC_TIMESPENT), createIndex(index, DC_TIMESPENT));
}

void IssueTableModel::save()
{
    Preferences pref;

    pref.beginGroup("IssueTableModel");
    pref.beginWriteArray("PersistentIssues");

    int ps_index = 0;
    foreach(AnnotatedJiraIssue aji, issues)
    {
        if (aji.persistent())
        {
            pref.setArrayIndex(ps_index);
            aji.save(pref);
            ++ps_index;
        }
    }

    pref.endArray();
    pref.endGroup();
}

void IssueTableModel::load()
{
    Preferences pref;
    pref.beginGroup("IssueTableModel");
    int count = pref.beginReadArray("PersistentIssues");
    QList<AnnotatedJiraIssue> local;

    for(int i = 0; i < count; ++i)
    {
        pref.setArrayIndex(i);        
        local << AnnotatedJiraIssue(pref);
    }

    mergeIssues(local);
    pref.endArray();
}

AnnotatedJiraIssue IssueTableModel::currentIssue() const
{
    AnnotatedJiraIssue result = JiraIssue();
    if (current_index != -1)
        result = issues.at(current_index);
    return result;
}

QStringList IssueTableModel::getIssueKeys() const
{
    QStringList issueKeys;

    foreach (const AnnotatedJiraIssue &issue, issues)
    {
        issueKeys << issue.issue.key;
    }

    return issueKeys;
}

IssuesSort::IssuesSort(QObject* parent /* = 0*/) : QSortFilterProxyModel(parent)
{
    setSortRole(IssueTableModel::SortRole);
}

bool IssuesSort::lessThan(const QModelIndex& left, const QModelIndex& right) const
{
    return (QSortFilterProxyModel::lessThan(left, right));
}
