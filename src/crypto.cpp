#include <QCryptographicHash>
#include <QRegExp>
#include "crypto.h"
#include "simplecrypt.h"

#include <algorithm>
using std::max;
using std::min;

namespace crypto {

QString encrypt(const QString& data, quint64 ck, char stable_char /* = '\x00'*/)
{
    SimpleCrypt sc(ck);
    return sc.encryptToString(data, stable_char);
}

QString decrypt(const QString& data, quint64 ck)
{
    SimpleCrypt sc(ck);
    return sc.decryptToString(data);
}

QString pkey(const QString& key)
{
    const quint64 reg_key = Q_UINT64_C(0xcccddf5fa3b631a2);
    QByteArray arr(encrypt(key, reg_key, '\x88').toStdString().c_str());
    QString res = QString(QCryptographicHash::hash(arr, QCryptographicHash::Md4).toHex()).toUpper().replace(QRegExp("[^A-Z0-9]"), "");
    res.resize(max(res.length(), 16));
    Q_ASSERT(!res.isEmpty());
    return res;
}

}
