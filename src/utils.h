#ifndef __UTILS__
#define __UTILS__

#include <QString>
#include <QObject>
#include <algorithm>

class utils : public QObject
{
    Q_OBJECT
private:
    utils();
public:
    const static quint64 common_key;
    static QString product_name;
    static QString productName();
    static QString productVersion();
    static QString getUserIDString();
    static QString getUserName();
    static QString userFriendlyDuration(qint32 seconds);
    static QString userFriendlyHours(qint32 seconds);
    static QString normalize(const QString&);
    static QString machineId();
    static QString worklogComment(const QString& comment);
    static qlonglong url2issueid(const QString&);

    template<typename ToColl, typename FromColl, typename Mapper>
    static ToColl fmap(const FromColl& source, const Mapper& f)
    {
        ToColl dst;
        std::transform(source.begin(), source.end(), std::inserter(dst, dst.end()), f);
        return dst;
    }
};


#endif // __UTILS__
