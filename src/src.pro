TEMPLATE = app
CONFIG += qt thread
QT += network
TARGET = jiraph

target.path = /usr/local/bin/
INSTALLS += target

unix:!macx {
  include(unixconf.pri)
}

win32: {
  include(winconf.pri)
}

include(../version.pri)
include(json/json.pri)
include(preferences/preferences.pri)
include(idledetector/idledetector.pri)
include(qtsingleapp/qtsingleapplication.pri)

LANG_PATH = lang
ICONS_PATH = icons

CONFIG(debug, debug|release):message(Project is built in DEBUG mode.)
CONFIG(release, debug|release):message(Project is built in RELEASE mode.)

# Disable debug output in release mode
CONFIG(release, debug|release) {
    message(Disabling debug output.)
    DEFINES += QT_NO_DEBUG_OUTPUT
}

CONFIG(local) {
    message(Project uses LOCAL_JSON input.)
    DEFINES += LOCAL_JSON
}

HEADERS +=  mainwindow.h \
            jiraclient.h \
            jiraresource.h \
            httpconnector.h \
            httprequest.h \
            error.h \
            simplecrypt.h \
            utils.h \
            issuetablemodel.h \
            commentdelegate.h \
            logworkdialog.h \
            filterlistmodel.h \
            sessionapplication.h \
            stacktrace.h \
            about.h \
            crypto.h \
            status_bar.h \
            idleissuedlg.h \
            hyperlinkdelegate.h

SOURCES +=  mainwindow.cpp \
            main.cpp \
            jiraclient.cpp \
            jiraresource.cpp \
            httpconnector.cpp \
            httprequest.cpp \
            simplecrypt.cpp \
            utils.cpp \
            issuetablemodel.cpp \
            commentdelegate.cpp \
            logworkdialog.cpp \
            filterlistmodel.cpp \
            sessionapplication.cpp \
            about.cpp \
            crypto.cpp \
            status_bar.cpp \
            idleissuedlg.cpp \
            hyperlinkdelegate.cpp

FORMS +=    mainwindow.ui \
            logworkdialog.ui \
            about.ui \
            status_bar.ui \
            idleissuedlg.ui

RESOURCES += icons/icons.qrc

mac {
    QMAKE_MACOSX_DEPLOYMENT_TARGET = 10.3
    ICON = icons/timer.icns
    QMAKE_INFO_PLIST = Info.plist
    QMAKE_LFLAGS += -framework Carbon -framework IOKit -framework AppKit
}
