#include <QtGui>
#include <QDebug>
#include <QMessageBox>
#include <QLineEdit>
#include <QAction>
#include <QComboBox>
#include <QToolBar>
#include <QInputDialog>

#if defined(Q_WS_X11) && defined(QT_DBUS_LIB)
#include <QDBusConnection>
#include "qtnotify/notifications.h"
#endif

#include "mainwindow.h"
#include "preferences/optionsdlg.h"
#include "preferences/preferences.h"
#include "json/json.h"
#include "jiraresource.h"
#include "commentdelegate.h"
#include "logworkdialog.h"
#include "utils.h"
#include "about.h"
#include "idleissuedlg.h"
#include "hyperlinkdelegate.h"

#define TIME_TRAY_BALLOON 5000

MainWindow::MainWindow(QWidget* parent/* = 0*/) : QMainWindow(parent), overIdle(-1)
{
    setupUi(this);
#ifdef LOCAL_JSON
    QList<JiraIssue> issues;
    // to start from root directory using: src/jiraph
#ifdef Q_WS_WIN
    QFile f("../../unittest/issuelist.json");
#else
    QFile f("unittest/issuelist.json");
#endif
    if (f.open(QFile::ReadOnly | QFile::Text))
    {
        QTextStream in(&f);
        QString input = in.readAll();
        bool ok;
        QtJson::JsonObject js = QtJson::parse(input, ok).toMap();

        if (!ok) qDebug("Unable to parse json");

        issues = json2issueList(js);
        qDebug() << "test issues: " << issues.size();
    }
    else
    {
        qDebug("Unable to open file with issues");
    }

#endif    
    statusBar = new status_bar(this, QMainWindow::statusBar());

    registerAction = NULL;  // need initialize to avoid removing from tool bar null action

    Preferences pref;
    setWinTitle();

    reminder = new QTimer(this);
    connect(reminder, SIGNAL(timeout()), this, SLOT(remind()));

    if (pref.getCheckReminder())
        reminder->start(pref.getSpinReminder()*1000);

    failsafe = new QTimer(this);
    connect(failsafe, SIGNAL(timeout()), this, SLOT(writeSettings()));

    if (pref.getCheckFailsafe())
        failsafe->start(pref.getSpinFailsafe()*1000);

    preferencesAction   = new QAction(QIcon(":preferences.png"), tr("Settings"), this);
    questionAction      = new QAction(QIcon(":question.png"), tr("About"), this);
    refreshIssuesAction = new QAction(QIcon(":refresh.png"), tr("Refresh issues"), this);

    currentIssueAction  = new QAction(tr("No issue selected"), this);
    startProgressAction = new QAction(QIcon(":play.png"), tr("Start progress"), this);
    stopProgressAction  = new QAction(QIcon(":stop.png"), tr("Stop progress"), this);
    logWorkAction       = new QAction(QIcon(":compose.png"), tr("Log work"), this);
    publishWorklogAction = new QAction(QIcon(":sound.png"), tr("Publish work log"), this);
    cancelWorklogAction = new QAction(QIcon(":subtract.png"), tr("Cancel work log"), this);

    currentIssueAction->setEnabled(false);
    startProgressAction->setEnabled(false);
    stopProgressAction->setEnabled(false);
    logWorkAction->setEnabled(false);
    publishWorklogAction->setEnabled(false);
    cancelWorklogAction->setEnabled(false);

    issuesToolBar = addToolBar(tr("Issues control tool bar"));

    issuesToolBar->addAction(startProgressAction);
    issuesToolBar->setObjectName("issuesToolBar");
    issuesToolBar->addAction(stopProgressAction);
    issuesToolBar->addSeparator();
    issuesToolBar->addAction(logWorkAction);
    issuesToolBar->addAction(publishWorklogAction);
    issuesToolBar->addAction(cancelWorklogAction);
    issuesToolBar->addSeparator();
    issuesToolBar->addAction(refreshIssuesAction);
    issuesToolBar->addAction(preferencesAction);
    issuesToolBar->addAction(questionAction);

    filterCombo = new QComboBox(this);
    refreshFiltersAction = new QAction(QIcon(":refresh.png"), tr("Refresh filters list"),this);
    filterEdit  = new QLineEdit(this);
    filterAction = new QAction(QIcon(":apply.png"), tr("Apply filter"), this);

    filtersToolBar = addToolBar(tr("Filters tool bar"));
    filtersToolBar->setObjectName("filtersToolBar");
    filtersToolBar->addWidget(filterCombo);
    filtersToolBar->addAction(refreshFiltersAction);
    filtersToolBar->addSeparator();
    filtersToolBar->addWidget(new QLabel(tr("Filter:")));
    filtersToolBar->addWidget(filterEdit);
    filtersToolBar->addAction(filterAction);

    minimizeAction = new QAction(QIcon(":minimize_window.png"), tr("Mi&nimize"), this);
    maximizeAction = new QAction(QIcon(":maximize_window.png"), tr("Ma&ximize"), this);
    restoreAction = new QAction(QIcon(":restore_window.png"), tr("&Restore"), this);
    quitAction = new QAction(QIcon(":circled_off.png"), tr("&Quit"), this);

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(currentIssueAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(startProgressAction);
    trayIconMenu->addAction(stopProgressAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(preferencesAction);
    trayIconMenu->addAction(questionAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(QIcon(":timer.ico"), this);
    trayIcon->setContextMenu(trayIconMenu);
    trayIcon->setVisible(pref.getShowInTray());

    issuesMenu = new QMenu(this);

    issuesMenu->addAction(refreshIssuesAction);
    issuesMenu->addSeparator();
    issuesMenu->addAction(startProgressAction);
    issuesMenu->addAction(stopProgressAction);
    issuesMenu->addSeparator();
    issuesMenu->addAction(logWorkAction);
    issuesMenu->addSeparator();
    issuesMenu->addAction(publishWorklogAction);
    issuesMenu->addAction(cancelWorklogAction);

    imodel.reset(new IssueTableModel());
    smodel.reset(new IssuesSort());
    smodel->setSourceModel(imodel.data());
    smodel->setDynamicSortFilter(true);
    issuesTable->setItemDelegateForColumn(IssueTableModel::DC_WORKLOG_TEXT, new CommentDelegate());
    issuesTable->setItemDelegateForColumn(IssueTableModel::DC_KEY, new HyperLinkDelegate(issuesTable));
    issuesTable->setModel(smodel.data());
    issuesTable->horizontalHeader()->setContextMenuPolicy(Qt::CustomContextMenu);
    issuesTable->horizontalHeader()->setMovable(true);

    session.reset(new JiraClient(pref.getServerURL(), pref.getUsername(), pref.getPassword()));
    idle.reset(new Psi::Idle());

    fmodel.reset(new FilterListModel());
    filterCombo->setModel(fmodel.data());

    readSettings();

    if (!pref.isRegistered())
    {
        registerAction      = new QAction(QIcon(":crown.png"), tr("Register your product"), this);
        issuesToolBar->addAction(registerAction);
        moneyPrayer = new QTimer();
        moneyPrayer->start(1000*1200);
        connect(moneyPrayer, SIGNAL(timeout()), this, SLOT(prayMoney()));
        connect(registerAction, SIGNAL(triggered()), this, SLOT(showAboutReg()));
    }

    filterAction->setEnabled(!filterEdit->text().isEmpty());

    // filters connections
    connect(refreshIssuesAction, SIGNAL(triggered()), this, SLOT(refreshIssues()));
    connect(preferencesAction, SIGNAL(triggered()), this, SLOT(preferences()));
    connect(refreshFiltersAction, SIGNAL(triggered()), this, SLOT(refreshFilters()));
    connect(questionAction, SIGNAL(triggered()), this, SLOT(showAbout()));    
    connect(filterEdit, SIGNAL(textChanged(QString)), this, SLOT(filterChanged(QString)));
    connect(filterEdit, SIGNAL(returnPressed()), this, SLOT(applyFilter()));
    connect(filterAction, SIGNAL(triggered()), this, SLOT(applyFilter()));
    connect(filterCombo, SIGNAL(currentIndexChanged(int)), this, SLOT(filtersComboIndexChanged(int)));


    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(minimize()));
    connect(maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));

    connect(trayIcon, SIGNAL(messageClicked()), this, SLOT(messageClicked()));
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));


    connect(startProgressAction, SIGNAL(triggered()), this, SLOT(startProgress()));
    connect(stopProgressAction, SIGNAL(triggered()), this, SLOT(stopProgress()));
    connect(logWorkAction, SIGNAL(triggered()), this, SLOT(logWork()));
    connect(publishWorklogAction, SIGNAL(triggered()), this, SLOT(publishWorklog()));
    connect(cancelWorklogAction, SIGNAL(triggered()), this, SLOT(cancelWorklog()));


    connect(issuesTable->horizontalHeader(), SIGNAL(customContextMenuRequested(const QPoint&)),
            this, SLOT(displayHSMenu(const QPoint&)));
    connect(issuesTable, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(issuesCtxMenu(QPoint)));
    connect(issuesTable->horizontalHeader(), SIGNAL(sortIndicatorChanged(int, Qt::SortOrder)),
                this, SLOT(sortIssuesRequested(int, Qt::SortOrder)));
    connect(issuesTable->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this,
            SLOT(issuesSelectionChanged(QItemSelection,QItemSelection)));
    connect(issuesTable, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(toggleProgress(QModelIndex)));
    connect(imodel.data(), SIGNAL(dataChanged(QModelIndex,QModelIndex)), this, SLOT(handleIssuesChanged(QModelIndex,QModelIndex)));


    connect(idle.data(), SIGNAL(secondsIdle(int)), this, SLOT(idleHandler(int)));
    idle->start();

    connect(session.data(), SIGNAL(issue(JiraIssue)), this, SLOT(updateIssue(JiraIssue)));
    connect(session.data(), SIGNAL(issueList(QList<JiraIssue>)), this, SLOT(mergeIssues(QList<JiraIssue>)));
    connect(session.data(), SIGNAL(filterList(QList<JiraFilter>)), this, SLOT(mergeFilters(QList<JiraFilter>)));
    connect(session.data(), SIGNAL(worklogAdded(JiraWorklog)), this, SLOT(worklogAdded(JiraWorklog)));
    connect(session.data(), SIGNAL(transitionAdded(QString)), this, SLOT(issueChanged(QString)));
    connect(session.data(), SIGNAL(error(Error)), this, SLOT(processError(Error)));
    connect(session.data(), SIGNAL(requestStarted(JiraRequest)), this, SLOT(requestStarted(JiraRequest)));
    connect(session.data(), SIGNAL(requestFinished(JiraRequest)), this, SLOT(requestFinished(JiraRequest)));

#ifdef LOCAL_JSON
    imodel->mergeIssues(utils::fmap<QList<AnnotatedJiraIssue> >(issues, &AnnotatedJiraIssue::fromJiraIssue));
#endif

    if (pref.getStartMinimized())
        minimize();

    trayIcon->setToolTip(status());
    show();

    if (!Preferences().readyToConnect())
    {
        showPreferences(OptionsDlg::CONNECTION_PAGE);
    }
    else
    {
        refreshIssues();
    }
}

MainWindow::~MainWindow()
{
    writeSettings();
}

void MainWindow::showPreferences(int page)
{
    OptionsDlg dlg(page, this);
    dlg.move(geometry().center().x() - dlg.geometry().width()/2,
             geometry().center().y() - dlg.geometry().height()/2);
    connect(&dlg, SIGNAL(madeChanges()), this, SLOT(optionsChanged()));

    if (dlg.exec() == QDialog::Accepted)
        refreshIssues();
}

void MainWindow::writeSettings()
{
    Preferences pref;
    pref.beginGroup(QString::fromUtf8("Preferences/MainWindow"));
    pref.setValue("State", saveState());
    pref.setValue("Geometry", saveGeometry());
    pref.setValue("IssueTable", issuesTable->horizontalHeader()->saveState());
    pref.setUserFilter(filterEdit->text());
    imodel->save();
    fmodel->save();
    pref.setFilterComboIndex(filterCombo->currentIndex());
    pref.endGroup();
    pref.sync();
}

void MainWindow::readSettings()
{
    Preferences pref;
    pref.beginGroup(QString::fromUtf8("Preferences/MainWindow"));

    if (pref.contains("State")) restoreState(pref.value("State").toByteArray());
    if (pref.contains("Geometry")) restoreGeometry(pref.value("Geometry").toByteArray());

    if (pref.contains("IssueTable"))
    {
        issuesTable->horizontalHeader()->restoreState(pref.value("IssueTable").toByteArray());
        issuesTable->horizontalHeader()->setMovable(true);
    }
    else
    {
        for(int i = 0; i < imodel->columnCount(); i++)
        {
            if (i != IssueTableModel::DC_KEY &&
                i != IssueTableModel::DC_SUMMARY  &&
                i != IssueTableModel::DC_STATUS &&
                i != IssueTableModel::DC_PRIOPRITY &&
                i != IssueTableModel::DC_WORKLOG &&
                i != IssueTableModel::DC_WORKLOG_TEXT)
                issuesTable->setColumnHidden(i, true);
        }

        issuesTable->horizontalHeader()->setSortIndicator(IssueTableModel::DC_KEY, Qt::AscendingOrder);
    }

    filterEdit->setText(pref.getUserFilter());

    imodel->load();
    fmodel->load();
    filterCombo->setCurrentIndex(pref.getFilterComboIndex());
    pref.endGroup();
}

void MainWindow::preferences()
{
    showPreferences(OptionsDlg::DESKTOP_PAGE);
}

void MainWindow::messageClicked()
{
    if (isHidden())
    {
        if (isMinimized())
        {
            if (isMaximized())
            {
                showMaximized();
            }
            else
            {
                showNormal();
            }
        }

        show();
        raise();
        activateWindow();
    }
    else
    {
        hide();
    }
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
 {
     switch (reason) {
     case QSystemTrayIcon::Context:
         minimizeAction->setEnabled(!isMinimized() && !isHidden());
         maximizeAction->setEnabled(!isMaximized() && !isHidden());
         restoreAction->setEnabled(isMaximized() || isMinimized() || isHidden());
        break;
     case QSystemTrayIcon::Trigger:
     case QSystemTrayIcon::DoubleClick:
         messageClicked();  // raise main window
         break;
     case QSystemTrayIcon::MiddleClick:
         break;
     default:
         ;
     }
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    Preferences pref;

    if (trayIcon->isVisible() && pref.closeToTray())
    {
        hide();
        event->ignore();
        return;
    }

    if (Preferences().getExitConfirmation())
    {
        if (!isVisible()) show();

        QMessageBox confirmBox(QMessageBox::Question, tr("Exiting jiraph"),
                             tr("Do you really want to quit jiraph?"),
                             QMessageBox::NoButton, this);

        QPushButton *noBtn = confirmBox.addButton(tr("No"), QMessageBox::NoRole);
        QPushButton *yesBtn = confirmBox.addButton(tr("Yes"), QMessageBox::YesRole);
        QPushButton *alwaysBtn = confirmBox.addButton(tr("Always"), QMessageBox::YesRole);

        confirmBox.setDefaultButton(yesBtn);
        confirmBox.exec();

        if (!confirmBox.clickedButton() || confirmBox.clickedButton() == noBtn)
        {
            event->ignore();
            return;
        }

        if (confirmBox.clickedButton() == alwaysBtn)
            Preferences().setExitConfirmation(false);
    }

    QApplication::quit();
}

void MainWindow::showEvent(QShowEvent * event)
{
    event->accept();
}

bool MainWindow::event(QEvent * e)
{
    switch(e->type())
    {
        case QEvent::WindowStateChange:
        {
            qDebug("Window change event");
            //Now check to see if the window is minimised
            if (isMinimized())
            {
                qDebug("minimisation");
                if (Preferences().minimizeToTray())
                {
                    qDebug("Minimize to Tray enabled, hiding!");
                    e->accept();
                    QTimer::singleShot(0, this, SLOT(hide()));
                    return true;
                }
            }
            break;
        }
#ifdef Q_WS_MAC
        /*
        case QEvent::ToolBarChange:
        {
            qDebug("MAC: Received a toolbar change event!");
            bool ret = QMainWindow::event(e);

            qDebug("MAC: new toolbar visibility is %d", !actionTop_tool_bar->isChecked());
            actionTop_tool_bar->toggle();
            Preferences().setToolbarDisplayed(actionTop_tool_bar->isChecked());
            return ret;
        }
        */
#endif
        default:
            break;
    }
    return QMainWindow::event(e);
}

void MainWindow::optionsChanged()
{
    Preferences pref;
    trayIcon->setVisible(pref.getShowInTray());
    restateIssuesToolBar(); // need for show/hide some buttons depends on comment policy
    session->reset(pref.getServerURL(), pref.getUsername(), pref.getPassword());
    if (pref.getCheckReminder())
    {
        reminder->setInterval(pref.getSpinReminder()*1000);
        if (!reminder->isActive()) reminder->start();
    }
    else
    {
        reminder->stop();
    }

    if (pref.getCheckFailsafe())
    {
        failsafe->setInterval(pref.getSpinFailsafe()*1000);
        if (!failsafe->isActive()) failsafe->start();
    }
    else
    {
        failsafe->stop();
    }
}

void MainWindow::minimize()
{    
    setWindowState(windowState() ^ Qt::WindowMinimized);
}

void MainWindow::processError(Error error)
{
    switch(error.type)
    {
    case Error::BAD_TRANSITION:
        showNotificationBaloon("Warning", error.message);
        break;
    default:
        QMessageBox::warning(this, "Error on request",
                             QString("%1 (code: %2)").arg(error.message, QString::number(error.code)));
        refreshFiltersAction->setEnabled(true);
        refreshIssuesAction->setEnabled(true);

        if (error.type == Error::AUTH_ERROR)
            showPreferences(OptionsDlg::CONNECTION_PAGE);
    }
}

void MainWindow::requestStarted(const JiraRequest& request)
{
    Preferences pref;
    statusBar->setRequest(request.description() + "...");
    statusBar->setLastJql(pref.getLastJql());
    statusBar->setLogin(pref.getUsername());
}

void MainWindow::requestFinished(const JiraRequest&)
{
    statusBar->setRequest("");
}

void MainWindow::addIssue()
{
    bool ok;
    QString text = QInputDialog::getText(this, tr("QInputDialog::getText()"),
                                         tr("Data"), QLineEdit::Normal, QString("") , &ok);
    if (ok && !text.isEmpty())
    {
        QStringList sl = text.split(" ");
        QList<AnnotatedJiraIssue> jlist;
        foreach(QString s, sl)
        {
            JiraIssue i;
            i.id = s.toInt();
            qDebug() << "generate issue: " << s;
            jlist.append(AnnotatedJiraIssue::fromJiraIssue(i));
        }

        imodel->mergeIssues(jlist);
    }
}

void MainWindow::updateIssue(const JiraIssue& issue)
{
    imodel->updateIssue(AnnotatedJiraIssue::fromJiraIssue(issue));
}

void MainWindow::mergeIssues(const QList<JiraIssue>& ilist)
{
    imodel->mergeIssues(utils::fmap<QList<AnnotatedJiraIssue> >(ilist, &AnnotatedJiraIssue::fromJiraIssue));
    refreshIssuesAction->setEnabled(true);
}

void MainWindow::mergeFilters(const QList<JiraFilter>& flist)
{
    fmodel->mergeFilters(flist);
    refreshFiltersAction->setEnabled(true);
}

void MainWindow::worklogAdded(const JiraWorklog& worklog)
{
    imodel->logWork(worklog.issueId(), worklog.timeSpentSeconds);
}

void MainWindow::issueChanged(const QString& issueKey)
{
    session->issue(issueKey);
}

void MainWindow::displayHSMenu(const QPoint&)
{
    QMenu hideshowColumn(this);
    hideshowColumn.setTitle(tr("Column visibility"));
    QList<QAction*> actions;

    for (int i=0; i < imodel->columnCount(); ++i)
    {
        QAction *myAct = hideshowColumn.addAction(
            imodel->headerData(i, Qt::Horizontal, Qt::DisplayRole).toString());
        myAct->setCheckable(true);
        myAct->setChecked(!issuesTable->isColumnHidden(i));
        actions.append(myAct);
    }

    // Call menu
    QAction *act = hideshowColumn.exec(QCursor::pos());

    if (act)
    {
        int col = actions.indexOf(act);
        Q_ASSERT(col >= 0);
        issuesTable->setColumnHidden(col, !issuesTable->isColumnHidden(col));
    }
}

void MainWindow::issuesCtxMenu(const QPoint& pos)
{
    QModelIndex index = s2i(issuesTable->indexAt(pos));
    if (index.isValid()) issuesMenu->exec(QCursor::pos());
}

void MainWindow::startProgress()
{
    const QModelIndexList ilist = issuesTable->selectionModel()->selectedRows();
    Q_ASSERT(ilist.size() == 1);
    foreach(QModelIndex i, ilist)
    {
        startProgress(s2i(i));
    }
}

void MainWindow::startProgress(const QModelIndex& issueIndex)
{
    stopProgress(imodel->currentProgress());

    imodel->startWorkLog(issueIndex);
    trayIcon->setIcon(QIcon(":timer_on.png"));

    if (Preferences().getToggleInProgress())
        session->addTransition(imodel->key(issueIndex), 4);
}

void MainWindow::stopProgress()
{
    const QModelIndexList ilist = issuesTable->selectionModel()->selectedRows();
    Q_ASSERT(ilist.size() == 1);
    foreach(QModelIndex i, ilist)
    {
        stopProgress(s2i(i));
    }
}

void MainWindow::stopProgress(const QModelIndex& issueIndex)
{
    if (issueIndex.isValid())
    {
        imodel->stopWorkLog(issueIndex);
        trayIcon->setIcon(QIcon(":timer.ico"));

        if (Preferences().getToggleInProgress())
            session->addTransition(imodel->key(issueIndex), 301);
    }
}

void MainWindow::toggleProgress(const QModelIndex& si)
{
    QModelIndex ii = s2i(si);
    if (ii.isValid() && ii.column() != IssueTableModel::DC_WORKLOG_TEXT)
    {
        if (imodel->currentProgress(ii))
            stopProgress(ii);
        else
            startProgress(ii);
    }
}

void MainWindow::logWork()
{
    const QModelIndexList ilist = issuesTable->selectionModel()->selectedRows();

    foreach(QModelIndex i, ilist)
    {
        LogWorkDialog lwdlg(session.data(), imodel->key(s2i(i)));
        lwdlg.exec();
    }
}

void MainWindow::publishWorklog()
{
    Preferences pref;
    const QModelIndexList ilist = issuesTable->selectionModel()->selectedRows();
    foreach(QModelIndex i, ilist)
    {        
        QString issueKey = imodel->key(s2i(i));
        qint32 timeSpentSeconds = imodel->worklog(s2i(i));
        QString timeSpent = QString::number(timeSpentSeconds/60) + "m";
        QString comment = imodel->worklogText(s2i(i));
        Q_ASSERT(pref.getRadioComment() != Preferences::RC_DENY || !comment.isEmpty()); // check comments policy

        if (comment.isEmpty() && pref.getRadioComment() == Preferences::RC_WARN)
        {
            QMessageBox::StandardButton reply;
            reply = QMessageBox::question(
                this, "Work log publish",
                tr("Comment for worklog  on ticket %1 is empty, continue?").arg(issueKey),
                QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);

            if (reply == QMessageBox::Cancel)   // skip all worlogs
                break;

            if (reply == QMessageBox::No)       // skip only current worklog
                continue;
        }

        session->addWorklog(issueKey, timeSpent, 0, utils::worklogComment(comment));
    }
}

void MainWindow::cancelWorklog()
{
    const QModelIndexList ilist = issuesTable->selectionModel()->selectedRows();
    foreach(QModelIndex i, ilist)
    {
        imodel->cancelWorkLog(s2i(i));
    }
}

QModelIndex MainWindow::s2i(const QModelIndex& index) const
{
    if (index.isValid())
    {
        Q_ASSERT(index.model() == smodel.data());
        return smodel->mapToSource(index);
    }

    return QModelIndex();
}


void MainWindow::sortIssuesRequested(int column, Qt::SortOrder order)
{
    smodel->sort(column, order);
}

void MainWindow::idleHandler(int idle_duration)
{
    Preferences pref;
    AnnotatedJiraIssue aIssue = imodel->currentIssue();

    if (pref.getCheckActivity() && aIssue.defined())
    {
        if (idle_duration >= pref.getSpinActivity())
            overIdle = idle_duration;

        if (idle_duration < pref.getSpinActivity() && overIdle != -1)
        {
            raise();

            // ask user            
            int local_idle = overIdle;
            IdleIssueDlg dlg(local_idle, this);
            QString idleRes = dlg.getIssueKey(imodel->getIssueKeys(), aIssue.issue.key);

            if (!idleRes.isEmpty())
            {
                // user wants log time
                if (idleRes != aIssue.issue.key)
                {
                    if (aIssue.defined())
                        imodel->updateCurrentWorklog(-local_idle);

                    LogWorkDialog lw(session.data(), idleRes, local_idle, this);
                    lw.exec();
                }
            }
            else
            {
                if (aIssue.defined())
                    imodel->updateCurrentWorklog(-local_idle);
            }

            overIdle = -1;  // reset idle
        }
    }
}

void MainWindow::filterChanged(const QString& text)
{
    filterAction->setEnabled(!text.isEmpty());
}

void MainWindow::refreshFilters()
{
    refreshFiltersAction->setEnabled(false);
    session->favouriteFilters();
}

void MainWindow::refreshIssues()
{
    Preferences pref;

    // last jql initialization in first time or when config was removed
    if (pref.getLastJql().isEmpty())
    {
        QVariant jql = filterCombo->itemData(filterCombo->currentIndex(), FilterListModel::JQLRole);
        Q_ASSERT(jql.isValid());
        pref.setLastJql(jql.toString());
    }

    refreshIssuesAction->setEnabled(false);
    session->searchIssues(pref.getLastJql());
    regNotification();
}

void MainWindow::applyFilter()
{
    Q_ASSERT(!filterEdit->text().isEmpty());
    Preferences().setLastJql(filterEdit->text());
    refreshIssues();
}

void MainWindow::filtersComboIndexChanged(int)
{
    QVariant jql = filterCombo->itemData(filterCombo->currentIndex(), FilterListModel::JQLRole);
    Q_ASSERT(jql.isValid());
    filterEdit->setText(jql.toString());
}

void MainWindow::restateIssuesToolBar()
{
    QModelIndexList selected = issuesTable->selectionModel()->selectedRows();

    if (selected.count() == 0)
    {
        currentIssueAction->setText(tr("No issues selected"));
        startProgressAction->setEnabled(false);
        stopProgressAction->setEnabled(false);
        logWorkAction->setEnabled(false);
        publishWorklogAction->setEnabled(false);
        cancelWorklogAction->setEnabled(false);
        return;
    }

    if (selected.size() > 1)
    {
        currentIssueAction->setText(tr("No issues selected"));
        startProgressAction->setEnabled(false);
        stopProgressAction->setEnabled(false);
        logWorkAction->setEnabled(false);
    }

    publishWorklogAction->setEnabled(allFilled(selected));
    cancelWorklogAction->setEnabled(anyLogged(selected));

    if (selected.size() == 1)
    {
        QModelIndex index = s2i(selected.at(0));
        currentIssueAction->setText(imodel->key(index));
        bool current_progress = imodel->currentProgress(index);
        logWorkAction->setEnabled(true);
        startProgressAction->setEnabled(!current_progress);
        stopProgressAction->setEnabled(current_progress);
    }
}

bool MainWindow::allFilled(const QModelIndexList& selected)
{
    bool all_filled = true;

    foreach(QModelIndex index, selected)
    {
        if (imodel->worklog(s2i(index)) < 60 ||
            (Preferences().getRadioComment() == Preferences::RC_DENY && imodel->worklogText(s2i(index)).isEmpty()))
        {
            all_filled = false;
            break;
        }
    }

    return all_filled;
}

bool MainWindow::anyLogged(const QModelIndexList& selected)
{
    bool any_logged = false;

    foreach(QModelIndex index, selected)
    {
        if (imodel->worklog(s2i(index)) > 0)
        {
            any_logged = true;
            break;
        }
    }

    return any_logged;
}

QString MainWindow::status()
{
    AnnotatedJiraIssue aIssue = imodel->currentIssue();
    QString result = aIssue.defined() ?
        QString("%1: %2\n"
                "Estimated: %3\n"
                "Remaining: %4\n"
                "Logged:    %5")
        .arg(aIssue.issue.key, aIssue.issue.summary,
             utils::userFriendlyDuration(aIssue.issue.timeoriginalestimate),
             utils::userFriendlyDuration(aIssue.issue.timeestimate),
             utils::userFriendlyDuration(aIssue.issue.timespent)):
        QString("No issue in progress");

    return result;
}

void MainWindow::issuesSelectionChanged(const QItemSelection&, const QItemSelection&)
{
   restateIssuesToolBar();
}

void MainWindow::handleIssuesChanged(const QModelIndex&, const QModelIndex&)
{
    restateIssuesToolBar();
    trayIcon->setToolTip(status());
}

void MainWindow::showNotificationBaloon(QString title, QString msg) const
{
    //if (!Preferences().useProgramNotification()) return;

#if defined(Q_WS_X11) && defined(QT_DBUS_LIB)
    org::freedesktop::Notifications notifications(
        "org.freedesktop.Notifications", "/org/freedesktop/Notifications", QDBusConnection::sessionBus());
    if (notifications.isValid()) {
        QVariantMap hints;
        hints["desktop-entry"] = "jiraph";
        QDBusPendingReply<uint> reply = notifications.Notify(
            "jiraph", 0, "jiraph", title, msg, QStringList(), hints, -1);
        reply.waitForFinished();
        if (!reply.isError())
            return;
    }
#endif
    if (trayIcon && QSystemTrayIcon::supportsMessages())
        trayIcon->showMessage(title, msg, QSystemTrayIcon::Information, TIME_TRAY_BALLOON);
}

void MainWindow::showAbout()
{
    AboutDlg a(this, false);
    a.exec();
    applyRegistration();
}

void MainWindow::showAboutReg()
{
    AboutDlg a(this, true);
    a.exec();
    applyRegistration();
}

void MainWindow::applyRegistration()
{
    if (Preferences().isRegistered() && registerAction)
    {
        issuesToolBar->removeAction(registerAction);
        delete registerAction;
        registerAction = NULL;
        setWinTitle();
    }
}

void MainWindow::setWinTitle()
{
    setWindowTitle(tr("%1 %2").arg(utils::productName()).arg(Preferences().productStatus()));
}

void MainWindow::regNotification()
{
    if (!Preferences().isRegistered())
    {
        QMessageBox::information(this, tr("Jiraph registration"), tr("Please, register your copy"));
    }
}

bool MainWindow::regAvailable()
{
    if (!Preferences().isRegistered())
    {
        QMessageBox::information(this, tr("Jiraph registration"), tr("This feature doesn't work in trial version"));
        return false;
    }

    return true;
}

void MainWindow::remind()
{
    AnnotatedJiraIssue aIssue = imodel->currentIssue();
    showNotificationBaloon(tr("Reminder"),
                           aIssue.defined()?tr("Do you work now on %1").arg(aIssue.issue.key):tr("Not currently tracked time"));
}

void MainWindow::prayMoney()
{
    if (!Preferences().isRegistered())
    {
        showNotificationBaloon(tr("Product registration"), tr("Please, register your copy"));
    }
    else
    {
        moneyPrayer->stop();
    }
}
