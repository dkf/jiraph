#include "commentdelegate.h"
#include <QLineEdit>
#include <QModelIndex>

CommentDelegate::CommentDelegate(QObject *parent) :
    QItemDelegate(parent)
{
}

QWidget* CommentDelegate::createEditor(QWidget *parent, const QStyleOptionViewItem& /*option*/, const QModelIndex &index) const
{
    return new QLineEdit(index.model()->data(index, Qt::EditRole).toString(), parent);
}

void CommentDelegate::setEditorData(QWidget *editor, const QModelIndex &index) const
{
    QLineEdit *textEditor = qobject_cast<QLineEdit*>(editor);

    if (textEditor)
    {
        textEditor->setText(index.model()->data(index, Qt::EditRole).toString());
    }
}

void CommentDelegate::setModelData(QWidget *editor, QAbstractItemModel *model, const QModelIndex &index) const
{
    QLineEdit *textEditor = qobject_cast<QLineEdit *>(editor);
    if (textEditor)
    {
        model->setData(index, textEditor->text());
    }
}
