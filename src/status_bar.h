#ifndef __STATUS_BAR__
#define __STATUS_BAR__

#include <QWidget>
#include "ui_status_bar.h"

QT_BEGIN_NAMESPACE
class QStatusBar;
QT_END_NAMESPACE

class status_bar : public QWidget, public Ui::status_bar
{
    Q_OBJECT
    QStatusBar* m_bar;
public:
    status_bar(QWidget *parent, QStatusBar *bar);
    ~status_bar();
    void setLogin(const QString&);
    void setRequest(const QString&);
    void setLastJql(const QString&);
    void reset();
};

#endif // __STATUS_BAR__
