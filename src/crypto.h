#ifndef __CRYPTO__
#define __CRYPTO__

#include <QString>

namespace crypto
{
QString encrypt(const QString& data, quint64 ck, char stable_char = '\x00');
QString decrypt(const QString& key, quint64 ck);
QString pkey(const QString& key);
}


#endif //__CRYPTO__
