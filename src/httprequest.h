#ifndef HTTPREQUEST_H
#define HTTPREQUEST_H

#include <QUrl>
#include <QPair>
#include <QNetworkRequest>

class HttpRequest
{
public:
    enum Method { GET, POST, PUT, HEAD, DELETE };
    typedef QList<QPair<QByteArray,QByteArray> > Headers;
    typedef QList<QPair<QString,QString> > Params;

    HttpRequest(HttpRequest::Method method, const QUrl& endpoint,
                const Headers& headers, const Params& params, const QByteArray& data = "");

    HttpRequest::Method method() const;
    const QUrl& endpoint() const;
    const Headers& headers() const { return m_headers; }
    const Params& params() const { return m_params; }
    const QByteArray& data() const { return m_data; }

    static QByteArray mkPostData(const Params& params);

protected:
    HttpRequest::Method m_method;
    QUrl m_endpoint;
    Headers m_headers;
    Params m_params;
    QByteArray m_data;
};

#endif // HTTPREQUEST_H
