#ifndef JIRACLIENT_H
#define JIRACLIENT_H

#include <QObject>

#include "httpconnector.h"
#include "httprequest.h"
#include "jiraresource.h"

class JiraRequest : public HttpRequest
{
public:
    enum Type { USER, ISSUE, ISSUE_LIST, FILTER_LIST, ADD_WORKLOG, ADD_TRANSITION, UNKNOWN };

    JiraRequest(Type type = UNKNOWN, const QString& desctiption = "",
                HttpRequest::Method method = HttpRequest::GET, const QUrl& endpoint = QUrl(),
                const Headers& headers = Headers(), const Params& params = Params(), const QByteArray& data = "");

    Type type() const { return m_type; }
    const QString& description() const { return m_description; }
    QString issueIdOrKey() const;

private:
    Type m_type;
    QString m_description;
};

class JiraClient : public QObject
{
    Q_OBJECT

public:
    JiraClient(const QString& server, const QString& username, const QString& password, int api_version = 2);
    void reset(const QString& server, const QString& username, const QString& password, int api_version = 2);

    void user(const QString& username);

    void searchIssues(const QString& jql, int startAt = 0, int maxResults = 50
                      /*const QString& fields = "*all", const QString& expand = ""*/);
    void issue(const QString& issueIdOrKey /*const QString& fields = "*all", const QString& expand = ""*/);

    void favouriteFilters(/*const QString& expand = ""*/);

    void addWorklog(const QString& issueIdOrKey, const QString& timeSpent,
                    qint32 timeSpentSeconds = 0, const QString& comment = "",
                    const QString& adjustEstimate = "", const QString& newEstimate = "",
                    const QString& reduceBy = "");

    void addTransition(const QString& issueIdOrKey, qlonglong transitionId);

    bool hasPendingRequests() const { return !m_pendingRequests.isEmpty(); }

signals:
    void user(JiraUser user);
    void issue(JiraIssue issue);
    void issueList(QList<JiraIssue> issues);
    void filterList(QList<JiraFilter> filters);
    void worklogAdded(JiraWorklog worklog);
    void transitionAdded(QString issueKey);
    void error(Error error);
    void warning(QString warning);

    void progress(qint64 bytesReceived, qint64 bytesTotal);
    void requestStarted(JiraRequest);
    void requestFinished(JiraRequest);

public slots:
    void requestFinished(const QByteArray& json);
    void requestError(Error error);

private:
    QUrl getURL(const QString& path) const;
    void sendRequest(const JiraRequest& request);
    void sendNextRequest();
    void sendRequestNow(const JiraRequest& request);

    JiraRequest pendingRequest() const;

    HttpConnector m_httpConnector;
    QString m_baseURL;
    HttpRequest::Headers m_headers;
    QQueue<JiraRequest> m_pendingRequests;
};

#endif // JIRACLIENT_H
