#include "hyperlinkdelegate.h"
#include "issuetablemodel.h"
#include <QLabel>
#include <QAbstractItemView>
#include <QUrl>

HyperLinkDelegate::HyperLinkDelegate(QObject *parent) :
    QItemDelegate(parent)
{
}

void HyperLinkDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{    
    Q_ASSERT(parent());
    QString name = index.data().toString();
    // this is a link to torrent tracker
    QAbstractItemView* view = dynamic_cast<QAbstractItemView*>(parent());
    Q_ASSERT(view);
    QSortFilterProxyModel* model = dynamic_cast<QSortFilterProxyModel*>(view->model());
    Q_ASSERT(model);

    QModelIndex self_index = model->mapToSource(model->index(index.row(), IssueTableModel::DC_SELF));
    Q_ASSERT(self_index.isValid());
    QLabel* label = NULL;

    if (!view->indexWidget(index))
    {
        // link isn't set yet
        label = new QLabel(view);
        view->setIndexWidget(index, label);
    }
    else
    {
        label = (QLabel*)(view->indexWidget(index));
    }

    QUrl url(self_index.data().toString());
    QUrl surl(Preferences().getServerURL());    // temporary solution - use current server path for build issues link
    QString link = url.scheme() + "://" + url.authority() + surl.path() + "/browse/" + name;
    QString lbText = "<tr><td><img src=\":hkey.png\" height=16 width=16></td><td><a href=\"" +
            link +  "\">&nbsp;" + name + "</a></td></tr>";
    if (lbText != label->text())
        label->setText(lbText);

    label->setOpenExternalLinks(true);
    label->setToolTip(link);

    drawDisplay(painter, option, option.rect, "");
}

