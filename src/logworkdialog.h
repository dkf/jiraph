#ifndef LOGWORKDIALOG_H
#define LOGWORKDIALOG_H

#include <QDialog>

namespace Ui {
class LogWorkDialog;
}

class JiraClient;
class LogWorkDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit LogWorkDialog(JiraClient* jira, const QString& key, qint32 duration = 0, QWidget *parent = 0);
    ~LogWorkDialog();

public slots:
    void accept();

private slots:
    void handleChangeTimeSpent(const QString& text);
    void restateControls();
private:
    Ui::LogWorkDialog *ui;
    JiraClient* jira;
    bool        goodWorkLog;
};

#endif // LOGWORKDIALOG_H
