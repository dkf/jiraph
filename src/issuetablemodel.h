#ifndef __ISSUE_TABLE_MODEL__
#define __ISSUE_TABLE_MODEL__

#include <QAbstractTableModel>
#include <QTimer>
#include <QSortFilterProxyModel>
#include "jiraclient.h"
#include "preferences.h"

// JiraIssue holder for save additional data on each issue
struct AnnotatedJiraIssue
{
    JiraIssue   issue;
    QString     worklogText;
    qint32     worklogAmount;

    static AnnotatedJiraIssue fromJiraIssue(const JiraIssue&);
    AnnotatedJiraIssue(const JiraIssue&);
    AnnotatedJiraIssue(const Preferences&); // restore from preferences
    void save(Preferences&); // save to preferences


    bool defined() const { return issue.defined(); }
    bool persistent() const  { return worklogAmount != 0;  }

    bool operator==(const JiraIssue& ji) { return issue == ji; }

    bool operator==(const AnnotatedJiraIssue& a) const { return issue == a.issue; }
    bool operator<(const AnnotatedJiraIssue& a) const { return issue < a.issue; }
    bool operator>(const AnnotatedJiraIssue& a) const { return issue > a.issue; }
    bool operator!=(const AnnotatedJiraIssue& a) const { return issue != a.issue; }

    bool operator==(qlonglong k) const { return issue.id == k; }
    bool operator<(qlonglong k) const { return issue.id < k; }
    bool operator>(qlonglong k) const { return issue.id > k; }
    bool operator!=(qlonglong k) const { return issue.id != k; }
};

class IssueTableModel : public QAbstractTableModel
{
    Q_OBJECT
private:
    int                             current_index;
    qlonglong                       current_ticket;
    QList<AnnotatedJiraIssue>       issues;
    QTimer                          timer;
    qlonglong id(const QModelIndex&) const;
    QString self(const QModelIndex&) const;
    QString summary(const QModelIndex&) const;
    QString description(const QModelIndex&) const;
    QString status(const QModelIndex&) const;
    QString priority(const QModelIndex&) const;
    qint32 timespent(const QModelIndex&) const;
    qint32 timestimate(const QModelIndex&) const;
    qint32 timeoriginalestimate(const QModelIndex&) const;
    QDateTime created(const QModelIndex&) const;
    QDateTime updated(const QModelIndex&) const;
    QDateTime duedate(const QModelIndex&) const;
    QDateTime lastviewed(const QModelIndex&) const;
    int itr2pos(const QList<AnnotatedJiraIssue>::const_iterator& itr) const;
    int ticketId2index(qlonglong) const;
public:
    enum Roles
    {
        SortRole = Qt::UserRole + 1
    };

    enum DisplayColumns
    {
        DC_ID = 0,
        DC_KEY,
        DC_SELF,
        DC_SUMMARY,
        DC_DESCRIPTION,
        DC_STATUS,
        DC_PRIOPRITY,
        DC_TIMESPENT,
        DC_TIMEESTIMATE,
        DC_TIMEORIGINALESTIMATE,
        DC_CREATED,
        DC_UPDATED,
        DC_DUEDATE,
        DC_LASTVIEWED,
        DC_WORKLOG,
        DC_WORKLOG_TEXT,
        DC_ASSIGNEE_SELF,
        DC_ASSIGNEE_NAME,
        DC_ASSIGNEE_EMAIL,
        DC_ASSIGNEE_DISPLAY_NAME,
        DC_REPORTER_SELF,
        DC_REPORTER_NAME,
        DC_REPORTER_EMAIL,
        DC_REPORTER_DISPLAY_NAME,
        DC_ISSUE_TYPE_NAME,
        DC_ISSUE_TYPE_SUBTASK,
        DC_END
    };

    IssueTableModel();
    int rowCount(const QModelIndex & parent = QModelIndex()) const;
    int columnCount(const QModelIndex & parent = QModelIndex()) const;
    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value,  int role = Qt::EditRole);
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    QString key(const QModelIndex&) const;
    qint32 worklog(const QModelIndex&) const;
    QString worklogText(const QModelIndex&) const;
    const Persona& assignee(const QModelIndex&) const;
    const Persona& reporter(const QModelIndex&) const;
    const IssueType& itype(const QModelIndex&) const;
    bool worklogged(const QModelIndex&) const;
    bool currentProgress(const QModelIndex&) const;
    QModelIndex currentProgress() const;
    void updateCurrentWorklog(qint32);

    // start new work log and stop all other
    void startWorkLog(const QModelIndex&);
    void stopWorkLog(const QModelIndex&);
    void cancelWorkLog(const QModelIndex&);
    void logWork(qlonglong issueId, qint32 timeSpentSeconds);
    void save();
    void load();
    AnnotatedJiraIssue currentIssue() const;
    QStringList getIssueKeys() const;
public slots:
    void updateIssue(const AnnotatedJiraIssue& issue);
    void mergeIssues(const QList<AnnotatedJiraIssue>& issues);
private slots:
    void updateWorkLog();
};


class IssuesSort : public QSortFilterProxyModel
{
    Q_OBJECT
public:
    IssuesSort(QObject* parent = 0);
    bool lessThan(const QModelIndex& left, const QModelIndex& right) const;
};

#endif
