#include <QApplication>
#include <QDir>
#include <QSplashScreen>
#include <QPainter>

#include <iostream>
#include <stdio.h>

#include "mainwindow.h"
#include "utils.h"
#include "sessionapplication.h"

#if defined(Q_WS_X11) || defined(Q_WS_MAC)
#include <signal.h>
#include <execinfo.h>
#include "stacktrace.h"
#endif

#include <QMessageBox>

#if defined(Q_WS_X11) || defined(Q_WS_MAC)

void sigintHandler(int)
{
    signal(SIGINT, 0);
    qDebug("Catching SIGINT, exiting cleanly");
    qApp->exit();
}

void sigtermHandler(int)
{
    signal(SIGTERM, 0);
    qDebug("Catching SIGTERM, exiting cleanly");
    qApp->exit();
}

void sigsegvHandler(int)
{
    signal(SIGABRT, 0);
    signal(SIGSEGV, 0);
    std::cerr << "\n\n*************************************************************\n";
    std::cerr << "Catching SIGSEGV, please report a bug at dkfsoft provide the following backtrace:\n";
    print_stacktrace();
    raise(SIGSEGV);
}

void sigabrtHandler(int)
{
    signal(SIGABRT, 0);
    signal(SIGSEGV, 0);
    std::cerr << "\n\n*************************************************************\n";
    std::cerr << "Catching SIGABRT, please report a dkfsoft provide the following backtrace:\n";
    print_stacktrace();
    raise(SIGABRT);
}

#endif

#ifdef Q_WS_WIN

void customMessageHandler(QtMsgType type, const char *msg)
{
    QString txt;
    switch (type) {
    case QtDebugMsg:
        txt = QString("Debug: %1").arg(msg);
        break;

    case QtWarningMsg:
        txt = QString("Warning: %1").arg(msg);
    break;
    case QtCriticalMsg:
        txt = QString("Critical: %1").arg(msg);
    break;
    case QtFatalMsg:
        txt = QString("Fatal: %1").arg(msg);
        abort();
    }

    QFile outFile("debuglog.txt");
    outFile.open(QIODevice::WriteOnly | QIODevice::Append);
    QTextStream ts(&outFile);
    ts << txt << endl;
}

#endif

int main(int argc, char *argv[])
{
#if defined(Q_WS_MAC)
    QDir dir(QApplication::applicationDirPath());
    dir.cdUp();
    dir.cd("plugins");
    QApplication::setLibraryPaths(QStringList(dir.absolutePath()));
#endif
    Preferences pref;
    QString uid = utils::getUserIDString();
    SessionApplication app(utils::product_name+uid, argc, argv);
    app.setApplicationName(utils::product_name);

    // process command line parameters
    if (QCoreApplication::arguments().contains("-v") || QCoreApplication::arguments().contains("--version"))
    {
        std::cout << utils::productVersion().toStdString() << std::endl;
        return 0;
    }

#ifdef Q_WS_WIN
    qInstallMsgHandler(customMessageHandler);
#endif

    // Check if qMule is already running for this user
    if (app.isRunning())
    {
        qDebug("Jiraph is already running for this user.");
        app.sendMessage("restore");
        return 0;
    }

    app.setQuitOnLastWindowClosed(false);

    // Load translation
    QString locale; // = pref.getLocale();
    //QTranslator qtTranslator;
    //QTranslator translator;

    if (locale.isEmpty())
    {
        locale = QLocale::system().name();
        qDebug("locale is empty, use %s", qPrintable(locale));

        /*pref.setLocale(locale);
        if (locale.isEmpty())
        {
            locale = "en_GB";
        }
        */
    }

    qDebug("Program locale: %s", qPrintable(locale));
    setlocale(LC_CTYPE, ""); // TODO - need attention

    /*
    QSplashScreen* splash = 0;

    if (pref.getSplashScreen())
    {
        QPixmap splash_img(":logo.png");
        //QPainter painter(&splash_img);
        //QString prog_name = "dkfsoft jiraph";
        //painter.setPen(QPen(Qt::black));
        //painter.setFont(QFont("Arial", 22, QFont::Black));
        //painter.drawText(175 - painter.fontMetrics().width(prog_name) / 2, 220, prog_name);
        //painter.drawText(175 - painter.fontMetrics().width(version) / 2, 220 + painter.fontMetrics().height(), version);

        splash = new QSplashScreen(splash_img, Qt::WindowStaysOnTopHint);
        splash->show();
        app.processEvents();
    }
*/

#if defined(Q_WS_X11) || defined(Q_WS_MAC)
    signal(SIGABRT, sigabrtHandler);
    signal(SIGTERM, sigtermHandler);
    signal(SIGINT, sigintHandler);
    signal(SIGSEGV, sigsegvHandler);
#endif
    MainWindow window;
    app.setActivationWindow(&window);
    return app.exec();
}

