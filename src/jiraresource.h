#ifndef JIRA_RESOURCE_H
#define JIRA_RESOURCE_H

#include <QPair>
#include <QDateTime>

#include "json/json.h"

template<typename T>
void extract(T& dst, const QtJson::JsonObject& source, const QString& field_name)
{
    dst = source[field_name].value<T>();
}

template<typename T>
T extract(const QtJson::JsonObject& source, const QString& field_name)
{
    return source[field_name].value<T>();
}

struct JiraUser
{
    QString name;
    QString emailAddress;
    QString displayName;
};

struct Persona
{
    QString self;
    QString name;
    QString email;
    QString displayName;
    bool operator==(const Persona& p) const {
        return self == p.self && name == p.name && email == p.email && displayName == p.displayName;
    }
};

struct IssueType
{
    QString self;
    qint32  id;
    QString description;
    QString name;
    bool    subtask;
    bool operator==(const IssueType& it) const {
        return  self == it.self &&
                id == it.id &&
                description == it.description &&
                name == it.name &&
                subtask == it.subtask;
    }
};

struct JiraIssue
{
    static const qlonglong UNDEFINED_ID;
    qlonglong id;
    QString key;
    QString self;

    QString summary;
    QString description;
    qint32 aggregatetimeestimate;
    qint32 aggregatetimeoriginalestimate;
    qint32 aggregatetimespent;

    qint32 timespent;
    qint32 timeestimate;
    qint32 timeoriginalestimate;
    qint32 workratio;

    QDateTime created;
    QDateTime updated;
    QDateTime duedate;
    QDateTime lastViewed;

    QPair<QString, QString> status;
    QPair<QString, QString> priority;
    QPair<qint32, QPair<qint32, qint32> >   progress; // precent, (total, progress)
    Persona assignee;
    Persona reporter;
    IssueType   issueType;

    JiraIssue();

    bool defined() const { return id != UNDEFINED_ID; }
    bool operator==(const JiraIssue& ji) const { return id == ji.id; }
    bool operator<(const JiraIssue& ji) const { return id < ji.id; }
    bool operator>(const JiraIssue& ji) const { return id > ji.id; }
    bool operator!=(const JiraIssue& ji) const { return id != ji.id; }

    bool equal(const JiraIssue& j)
    {
        return  (id  == j.id &&
                key == j.key &&
                self == j.self &&
                summary == j.summary &&
                description == j.description &&
                aggregatetimeestimate == j.aggregatetimeestimate &&
                aggregatetimeoriginalestimate == j.aggregatetimeoriginalestimate &&
                aggregatetimespent == j.aggregatetimespent &&
                timespent == j.timespent &&
                timeestimate  == j.timeestimate &&
                timeoriginalestimate == j.timeoriginalestimate &&
                workratio == j.workratio &&
                created == j.created &&
                updated == j.updated &&
                duedate == j.duedate &&
                lastViewed == j.lastViewed &&
                status == j.status &&
                priority == j.priority &&
                progress == j.progress &&
                reporter == j.reporter &&
                assignee == j.assignee &&
                issueType == j.issueType);
    }
};

struct JiraFilter
{
    static const qint32 SELF_FILTER_ID;
    static const qint32 UNDEFINED_FILTER_ID;
    qint32 id;
    QString name;
    QString self;
    QString jql;
    QString viewUrl;
    QString searchUrl;

    JiraFilter();
    JiraFilter(qint32 _id, const QString& _name, const QString& _self, const QString& _jql);
    bool operator==(const JiraFilter& jf) const { return id == jf.id; }
    bool operator>(const JiraFilter& jf) const { return id > jf.id; }
    bool operator<(const JiraFilter& jf) const { return id < jf.id; }
    bool operator!=(const JiraFilter& jf) const { return id != jf.id; }
    bool equal(const JiraFilter& jf) const
    {
        return  id == jf.id &&
                name == jf.name &&
                self == jf.self &&
                jql  == jf.jql &&
                viewUrl == jf.viewUrl &&
                searchUrl == jf.searchUrl;
    }
};

struct JiraPendingWorklog
{
    JiraPendingWorklog(const QString& id, const QString& spent, qint32 spentSeconds, const QString& comm):
        issueIdOrKey(id), timeSpent(spent), timeSpentSeconds(spentSeconds), comment(comm) {}
    QString issueIdOrKey;
    QString timeSpent; // "3h 20m"
    qint32 timeSpentSeconds;
    QString comment;
};

struct JiraWorklog
{
    qint32 id;
    QString self;
    QString comment;
    QString timeSpent;
    qint32 timeSpentSeconds;

    qlonglong issueId() const; // jira/rest/api/2/issue/10010/worklog/10000 -> 10010
};

extern JiraUser json2user(const QtJson::JsonObject&);

extern JiraIssue json2issue(const QtJson::JsonObject&);
extern QList<JiraIssue> json2issueList(const QVariant&);

extern JiraFilter json2filter(const QtJson::JsonObject&);
extern QList<JiraFilter> json2filterList(const QVariant&);

extern QtJson::JsonObject pendingWorklog2json(const JiraPendingWorklog&);

extern JiraWorklog json2worklog(const QtJson::JsonObject&);

#endif
