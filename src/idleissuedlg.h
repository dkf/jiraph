#ifndef IDLEISSUEDLG_H_
#define IDLEISSUEDLG_H_

#include <QtGui/QDialog>
#include <QPair>
#include <QTimer>

class QTimer;
//-----------------------------------------------------------
class Ui_IdleIssueDlg;
//-----------------------------------------------------------
/// Dialog allows to select issue key from the list after the idle period
class IdleIssueDlg: public QDialog
{
    Q_OBJECT
private:
    Ui_IdleIssueDlg *ui;
    QTimer tm;

private:
    // hide exec() from being executed by user
    int exec();
    int& current_idle;

private:
    void updateIdle();

public:
    IdleIssueDlg(int& idle, QWidget* parent = 0);
    virtual ~IdleIssueDlg();

    /// Returns selected issue key or empty string
    QString getIssueKey(const QStringList &issues, const QString &currentIssue);

private slots:
    void onTextChanged(const QString &text);
    void onTimer();
};
//-----------------------------------------------------------
#endif /* MASSEDITDLG_H_ */ 
