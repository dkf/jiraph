#ifndef HYPERLINKDELEGATE_H
#define HYPERLINKDELEGATE_H

#include <QItemDelegate>
#include <QPainter>
#include <QTextDocument>

class HyperLinkDelegate : public QItemDelegate
{
    Q_OBJECT
public:
    explicit HyperLinkDelegate(QObject *parent = 0);
    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
signals:
    
public slots:
    
};

#endif // HYPERLINKDELEGATE_H
